![licence](https://img.shields.io/badge/license-MIT-green) ![documentation](https://img.shields.io/badge/documentation-yes-blue)

# __MIPAN - Mutual Information based Privacy Adversarial Networks__

<div align="center">
    <img src=".icon/mipan_logo.png" width="300" height=300/>
</div>

## Overview
This package provides the software used to run the [experiments](experiments) 
in the paper [_Optimal Obfuscation Mechanisms via Machine Learning_](https://arxiv.org/abs/1904.01059)
by Marco Romanelli[^1], Konstantinos Chatzikokolakis and Catuscia Palamidessi[^2].
The whole pipeline has been uploaded as well as the files necessary to run the
exact same experiments in the paper. The paper has been published at [CSF2020](https://www.ieee-security.org/TC/CSF2020/).

[^1]: Funded by an Inria-CORDI scholarship.
[^2]: The work of Catuscia Palamidessi was partially funded by the ERC grant [Hypatia](https://project.inria.fr/hypatia/) under the European Union’s Horizon 2020 research and innovation programme. Grant agreement 835294.

We consider the problem of obfuscating sensitive information while preserving utility. Given that an analytical solution is often not feasible because of un-scalability and because the background knowledge may be too complicated to determine, we propose an approach based on machine learning, inspired by the _GAN (Generative Adversarial Network)_ paradigm. The idea is to set up two nets: the generator, that tries to produce an optimal obfuscation mechanism to protect the data, and the classifier, that tries to de-obfuscate the data. By letting the two nets compete against each other, the mechanism improves its degree of protection, until an equilibrium is reached. We apply our method to the case of location privacy, and we perform experiments on synthetic data and on real data from the _Gowalla_ dataset
(https://snap.stanford.edu/data/loc-gowalla_totalCheckins.txt.gz). We evaluate the privacy of the mechanism not only by its capacity to defeat the classifier, but also in terms of the Bayes error, which represents the strongest possible adversary. We compare the privacy-utility tradeoff of our method with that of the planar Laplace mechanism used in geo-indistinguishability, showing favorable results.

The scheme below represents the adverarial networks:

<!---![alt text](img/img_prova.png)--->
<img src="/model.png" alt="drawing" width="500"/>

where

<!---!* $`y`$ is an identifier for the user located in $`\underline{x}`$, a user ID;
* $`\underline{x}`$ is the input pattern from a dataset of real location 
checkpoints, it has two dimensions: a latitude and a longitude that represent 
the real location of user $`y`$;
* $`\underline{s}`$: two random numbers $`\in[0,1)`$; they respectively 
represent an angle seed and a radius seed and they are needed to compute 
obfuscated locations in polar coordinates; 
* $`\underline{x}^*`$ is the output of the _G_ net; it is a tuple 
(latitude, longitude) after the obfuscation of the original location, 
i.e. $`g(\underline{x}, \underline{s})=\underline{x}^*`$;
* $`c`$ is the function performed by the _C_ net, it is basically a 
classification function to retrieve the relative user id given a location;
* $`y^*`$ is the output of the _C_ net, it is the id that the classifier 
associates with the given location $`\underline{x}^*`$.--->
* $`x`$, $`y`$, $`z`$ and $`w`$ are instances of the random variables $`X`$,
$`Y`$, $`Z`$ and $`W`$ respectively;
* $`X`$: the sensitive information that the users wishes to conceal;
* $`W`$: the useful information with respect to some service provider and to the intended notion of utility,
* $`Z`$: the information made visible to the service provider,  which may be intercepted by some  attacker, and 
* $`Y`$: the information inferred by the attacker;
* $`s`$ (seed)  is a randomly-generated number $`\in[0,1)`$;
* $`g`$ is the function learnt by $`G`$, and it represents an obfuscation 
mechanism $`P_{Z|W}`$, the input $`s`$ provides the randomness needed to 
generate random noise. It is necessary because a neural network in itself is deterministic; 
* $`c`$ is the classification learnt by $`C`$, and it corresponds to $`P_{Y|Z}`$.

### Software and hardware requirements
The code has been developed using Python 2.7 and the libraries can easily be 
installed through Anaconda. The experiments have been run on a server equipped 
with 4 nVidia GeForce GTX 1080 Ti GPU cards. No tests have been run on only CPU 
architectures and so there is not support for that. We used TensorFlow version 
1.8.0 and Keras with TensorFlow backend version 2.1.6. 

## Datasets
The code inside the folder _PAN_dataset_creation_ lets the user create both an
artificial dataset and a dataset created starting from the _Gowalla_ dataset. Both
the dataset are created given a central point, a square centered on the central
point and a bigger squared, still center on the central point, representing the area
on which we are okay to scatter noisy points. The execution of the main file will
let the user obtain these two datasets. Whenever we run the main function in this
folder the following command line arguments must be passed as well:
* sys.argv[1] is the directory for synthetic data
* sys.argv[2] is the directory for real data
* sys.argv[3] absolute path to original .txt dataset file for Gowalla
* sys.argv[4] is the epsilon
* sys.argv[5] is the central latitude
* sys.argv[6] is the central longitude
* sys.argv[7] is the side length
* sys.argv[8] is the noisy side length
* sys.argv[9] is the cardinality of the users of interest
* sys.argv[10] is the synthetic side length
* sys.argv[11] is the synthetic epsilon for creating a noise of scattered 
original points around the vertices. 

We consider the following to be one of the best designs for our dataset: 8 columns
storing in order the original latitude, the original longitude, the 

In the [/Datasets/synthetic_data/](Datasets/synthetic_data) (synthetic data which 
represents a typical worst case scenario where the laplacian method cannot perform extremely 
well) and [/Datasets/real_data/](Datasets/real_data) (a portion of the data from _Gowalla_) folders, 
you can find the datasets we used in order to rerun the same experiments we run. 
There are different kinds of file. For the sake of simplicity we suggest that you 
use and create files like those tagged with the _ADV_ label. A file of this kind is 
an 8-colums pandas Dataframe containing:
1. the scaled original latitude
2. the scaled original latitude
3. the seed for the angle in the laplacian polar transformation
4. the seed for the radius in the laplacian polar transformation
5. the scaled noisy laplacian latitude
6. the scaled noisy laplacian longitude
7. the user id corresponding to the original position
8. the unique id for the original position.

## Saved models
In the [/standalone_saved_models](standalone_saved_models) folder we provide
three network models that can be used:
* _classifier_opt_opposite_MI_ is the model of a classifier that maximises the
mutual information between the supervision and the prediction
* _discriminator_CE_loss_ is a classifier that minimizes the element wise
cross-entropy between the supervision and the prediction
* _generator_3hl_100x100x100_ is the generator at step 0, i.e. the laplacian noise
generator with privacy parameter $`\epsilon=\frac{ln(2)}{100}`$.

To load a certain model it is possible to use the Keras method 
```python
keras.models.load_model(filepath)
```

## Standalone discriminator network
The code stored in the [/standalone_discriminator](standalone_discriminator) folder
will let you run experiments using the same discriminator used for the experiments 
in the paper. Both original locations and noisy locations can be classified using this 
classifier. This code will let you choose the GPUs you would like to be visible 
for this script and it will also let you choose the max amount of memory dedicated to
the task.
When run, the 
[/standalone_discriminator/discriminator_net.py](standalone_discriminator/discriminator_net.py)
file needs several command line inputs:
* sys.argv[1] is the batch size
* sys.argv[2] is the number of epochs
* sys.argv[3] is the GPU card id
* sys.argv[4] is the max amount of GPU memory to be used 
* sys.argv[5] is the absolute path to the training set (for instance a file like 
[/Datasets/synthetic_data/synthetic_training_set_standalone_ADV_input](Datasets/synthetic_data/synthetic_training_set_standalone_classifier_input))
* sys.argv[6] is the absolute path to test set (for instance a file like 
[/Datasets/synthetic_data/synthetic_test_set_standalone_ADV_input](Datasets/synthetic_data/synthetic_test_set_standalone_classifier_input)) 
* sys.argv[7] is the absolute path to the folder where we want the results to 
be stored (the files in this folder stores the values for losses and metrics at each epoch, 
by loading and reading them it is possible to plot the progress of the experiments)
* sys.argv[8] is the column of the dataset where the id of the users are stored 
(the classes, so column 6 for the datasets above)
* sys.argv[9] is the column where the latitude of interest is (so column 0 for
the datasets above)
* sys.argv[10] is the column where the longitude of interest is (so column 1 for
the datasets above)
* sys.argv[11] is the absolute path to the model of classifier we would like to 
load (it is always possible to create a new one using the _build_classifier_
function).

## Standalone adversarial network
This part of the code can be used to train the adversarial network in order to 
obtain a generator which can produce a new noise hopefully more efficient than
the laplacian noise, i.e. it can fool the discriminator more (lower metrics values)
than the laplacian noise and achieve a better (lower) value for the utility 
loss function. The function _adv_euclid_280_meters_ which measures the utility loss can be updated 
if you want to use a different distance by replacing the scalar 280 in the line 
```python
me = me - 280
```
in the file [/standalone_adversarial/adv_net.py](standalone_adversarial/adv_net.py).

The file mentioned above must be run with the following command line arguments:
* sys.argv[1] is the batch size for G
* sys.argv[2] is the number of epochs for G
* sys.argv[3] is the batch size for C
* sys.argv[4] is the number of epochs for C
* sys.argv[5] is value for the weight of the utility loss (avg euclidean distance
between original and noisy positions)
* sys.argv[6] is the value for the weight of the privacy (mutual information)
* sys.argv[7] is the GPU card id
* sys.argv[8] is the max amount of GPU memory to be used 
* sys.argv[9] is the learning rate for the adv network
* sys.argv[10] is the absolute path to the input file for the training set 
(for instance a file like 
[/Datasets/real_data/adv_selected_train_df](Datasets/real_data/adv_selected_train_df))
* sys.argv[11] is the absolute path to the input file for the test set (for instance a file like 
[/Datasets/real_data/adv_selected_test_df](Datasets/real_data/adv_selected_test_df))
* sys.argv[12] is the absolute path to the file of the discriminator model file
* sys.argv[13] is the absolute path to the file of the generator model file
* sys.argv[14] is the absolute path to the folder where all the results will be 
stored (the files in this folder stores the values for losses and metrics at each epoch, 
by loading and reading them it is possible to plot the progress of the experiments).

This script saves files of the kind "adv_net_model_WEIGHTS" at the end of each iteration. These 
files save the weights of the adversarial architecture after each iteration so
that they can be reloaded on an equivalent architecture to obtain the noise generator 
we are interested in (we provide the files of
interest for out experiments). To load the weights on a certain architecture it is possible to
use the following command:
```python
model.load_weights('path_to_weights')
```
where model is the variable which represents the instantiated network with the adversatrial
architecture described in the paper and *path_to_weights* is the path to file
we used to store the weight relative to a certain archotecture of interest.

## Bayes error computation
The script [/Bayes_error/compute_bayes_error.py](Bayes_error/compute_bayes_error) is useful
to compute the Bayes error of a Bayesian classifier. It takes in input a pandas dataframe 
with 4 columns: scaled latitude, scaled longitude, user id and location id. This function
divides the square of interest in cells and computes the bayes error according to the 
formula in the paper choosing for each cell the class that occurs most frequently.
Whenever we run this script the following command line arguments must be passed as well:
* sys.argv[1] cell side length in meters
* sys.argv[2] absolute path to the input file
* sys.argv[3] is the GPU card id
* sys.argv[4] is the max amount of GPU memory to be used
* sys.argv[5] number of repetitions to obtain many obfuscated locations from an original one with different random seeds

---

###### Note for the users
The folder  [/img](img) contains all the plots in the paper and it also contains the
plots of the training which have not been included in the paper because of the limited 
space at our disposal. The plots are in different folders, each one corresponding to 
one experiment.

Some users, trying to reproduce the results in the paper mentioned above, might be surprised
when s/he will not find the same exact values. This mainly depends on:
* different approximations in different machines;
* some randomization is present in our procedure, in fact the elements of the validation
sets are always randomly chosen (and consequently also the training data too) 
at runtime as a design style decision. This can yield slightly 
different results especially when we are not dealing with thousands of samples;
* anyway the differences are not supposed to be huge.
* one possible solution is to iterate the experiments several times and check the output results
are comparable, otherwise it is possible to curb the influence of random initialization by setting
the randomization seeds at the beginning of the run files as follows:
```python
from numpy.random import seed
seed(1)
from tensorflow import set_random_seed
set_random_seed(2)
```

Now, while for the training of the adversarial we reset the valiadation set at the beginning of each iteration
to let the algorithm to increase the amount of randomization in the learning procedure, for the evaluations of the discriminators the validation set is decided
once and forever at the beginning opf the experiment. In this case if the user does not want
to go ahead with the random choice of the elements of the validation set, it is possible to look for the 
files named *index_in_train_for_validation.pkl*. These files are in different folders 
corresponding to the 4 different experiments (classification without noise for synthetic data,
classification with laplacian noise for synthetic data, 
classification without noise for real data, classification with laplacian 
noise for real data). Each of these files is a pickled list of indexes of elements selected 
to be used as validation data. To use them it is necessary to load them
```python
with open(inputFile) as f:
    loaded_obj = pickle.load(open(f, 'rb'))
```
and substitute them for the result of the function 
*datasetSplit(supervisionDiscriminator_mat, testPercentage, valPercentage)*.

After many iterations (in the order of days of training) the whole algorithm might 
sensibly slow down because of the way Tensorflow is built: at each iteration
the graph of the network grows and it might become heavy to handle. To avoid unnecessary
waste of time when it becomes too slow we suggest to stop the training and
restart it after loading the adversarial network weights updated to the last iteration.
This should mimic the behaviour imposed by the function *tf.keras.backend.clear_session*. 
We plan to create a more user friendly solution in the future but for the proposed 
experiments this issue should not be a problem (at least is was not much of a problem 
for us).


## Optimal

An  auxiliary program is also included that computes the **optimal** obfuscation
mechanism via linear programming. This is the mechanism that minimizes the
expected quality loss, for a given privacy constraint in terms of the Bayes-risk
of an optimal adversary.

To compile
```
cd optimal
./compile
```
To run
```
./build/samples/geo-optimal-given-min-bayes-risk <width> <height> <cell_size> <min_bayes_risk>
```

The program uses a `width` x `height` grid of given `cell_size`. The loss function
is Euclidean and `min_bayes_risk` is the privacy contraint.

The program prints the privacy and utility of the solution, and saves the mechanism
found in a file named `solution.txt`.


