## Experiments
In this section we are going to show and briefly comment on the reports of the 
training. The experiments are shown in the same order as they appear on the paper. 
We first run experiments on a __synthetic__ dataset, meant to clearly show the 
advantages of our approach. Here is a plot of the synthetic points the we use 
(after splitting them as described in the paper) for training and validation 
purposes (on the left) and for testing (on the right).

<figure>
    <img 
        src="/experiments/synthetic/exp1/no_noise/b.png" 
        alt="Trainvaldata" height="360" width="480"
    >
    <figcaption>
        Fig.1 - Synthetic locations used for training and validation.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/no_noise/b 2.png" 
        alt="Tesdata" height="360" width="480"
    >
    <figcaption>
        Fig.2 - Synthetic locations used for testing.
    </figcaption>
</figure>

We then move to a real world set of data collected 
from the Gowalla dataset.

<figure>
    <img 
        src="/experiments/Gowalla/exp3/no_noise/b.png" 
        alt="Trainvaldata" height="360" width="480"
    >
    <figcaption>
        Fig.3 - Gowalla data locations used for training and validation.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/no_noise/b 2.png" 
        alt="Tesdata" height="360" width="480"
    >
    <figcaption>
        Fig.4 - Gowalla data locations used for testing.
    </figcaption>
</figure>

Before we start adding noise, we asses the prediction capabilities of the __C__ 
network on these sets of data. We show the plots of the learning curves here. 

<figure>
    <img 
        src="/experiments/synthetic/exp1/no_noise/results_standalone_discriminator_no_noise_bs_32_noe_100_Classifier_loss_.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.5 - Classification loss over the synthetic training and validation data.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/no_noise/results_standalone_discriminator_no_noise_bs_32_noe_100_Discriminator_categ_acc_.png" 
        alt="Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.6 - Classification accuracy over the synthetic training and validation data.
    </figcaption>
</figure>

These performances remain unalterd on the test set.

We run the same experiment on the Gowalla data.

<figure>
    <img 
        src="/experiments/Gowalla/exp3/no_noise/results_standalone_discriminator_no_noise_bs_32_noe_300_Classifier_loss_.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.7 - Classification loss over the Gowalla training and validation data.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/no_noise/results_standalone_discriminator_no_noise_bs_32_noe_300_Discriminator_categ_acc_.png" 
        alt="Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.8 - Classification accuracy over the Gowalla training and validation data.
    </figcaption>
</figure>

The detailed results are:

<figure>
    <img 
        src="/experiments/Gowalla/exp3/no_noise/gow_no_noise_res.png" 
        alt="Loss class" height="69.5" width="254.25"
    >
    <figcaption>
        Fig.9 - Classification results over all the sets of Gowalla data.
    </figcaption>
</figure>

### Experiment 1 - Synthetic data
We inject the laplacian noise according to what has been reported on the paper.
We obtain a new location distribution.
<figure>
    <img 
        src="/experiments/synthetic/exp1/laplacian_noise/a.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.10 - Distribution of the synthetic training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/laplacian_noise/a 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.11 - Distribution of the synthetic test data after the Laplacian noise injection.
    </figcaption>
</figure>

<figure>
    <img 
        src="/experiments/synthetic/exp1/laplacian_noise/results_standalone_discriminator_yes_noise_bs_32_noe_100_Discriminator_loss_.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.12 - Classification loss over the synthetic training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/laplacian_noise/results_standalone_discriminator_yes_noise_bs_32_noe_100_Discriminator_categ_acc_.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.13 - Classification accuracy over the synthetic training and validation data after the Laplacian noise injection.
    </figcaption>
</figure>

The complete results are shown in the following table
<figure>
    <img 
        src="/experiments/synthetic/exp1/laplacian_noise/lap_noise_res.png" 
        alt="Loss class" height="69.5" width="254.25"
    >
    <figcaption>
        Fig.14 - Classification results over all the sets of synthetic data after the Laplacian noise injection.
    </figcaption>
</figure>
We now train our adversarial paradigm.
<figure>
    <img 
        src="/experiments/synthetic/exp1/adversarial_and_G_noise/results_adversarial_bsG_COND_128_noeG_100_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_categ_acc.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.15 - Classification accuracy over the synthetic training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/adversarial_and_G_noise/results_adversarial_bsG_COND_128_noeG_100_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_loss.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.16 - Classification loss over the synthetic training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/adversarial_and_G_noise/results_adversarial_bsG_COND_128_noeG_100_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_discr_loss.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.17 - Privacy loss over the synthetic training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/adversarial_and_G_noise/results_adversarial_bsG_COND_128_noeG_100_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_gen_ANN_loss.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.18 - Utility loss over the synthetic training and validation during the adversarial training.
    </figcaption>
</figure>
From this experiments we obtain a generator wich gives this new noisy distribution.
<figure>
    <img 
        src="/experiments/synthetic/exp1/adversarial_and_G_noise/c.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.19 - Noisy location distribution generated by or mechanism over the training and validation data.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp1/adversarial_and_G_noise/c 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.20 - Noisy location distribution generated by or mechanism over the test data.
    </figcaption>
</figure>

With this we get to 0.25 classification accuracy which is optimal in this case scenario from the noise injection standpoont. For further details check the paper.

## Experiment 2 - Synthetic data
We repeat the same experiment with a stricter utility constraint.
We inject the laplacian noise according to what has been reported on the paper.
We obtain a new location distribution.
<figure>
    <img 
        src="/experiments/synthetic/exp2/laplacian_noise/a.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.21 - Distribution of the synthetic training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp2/laplacian_noise/a 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.22 - Distribution of the synthetic test data after the Laplacian noise injection.
    </figcaption>
</figure>

<figure>
    <img 
        src="/experiments/synthetic/exp2/laplacian_noise/results_standalone_discriminator_yes_noise_bs_32_noe_100_Discriminator_loss_.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.23 - Classification loss over the synthetic training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp2/laplacian_noise/results_standalone_discriminator_yes_noise_bs_32_noe_100_Discriminator_categ_acc_.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.24 - Classification accuracy over the synthetic training and validation data after the Laplacian noise injection.
    </figcaption>
</figure>

The complete results are shown in the following table
<figure>
    <img 
        src="/experiments/synthetic/exp2/laplacian_noise/res_lap.png" 
        alt="Loss class" height="69.5" width="254.25"
    >
    <figcaption>
        Fig.25 - Classification results over all the sets of synthetic data after the Laplacian noise injection.
    </figcaption>
</figure>
We now train our adversarial paradigm.
<figure>
    <img 
        src="/experiments/synthetic/exp2/adversarial_adnd_G_noise/results_adversarial_bsG_COND_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_categ_acc.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.26 - Classification accuracy over the synthetic training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp2/adversarial_adnd_G_noise/results_adversarial_bsG_COND_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_loss.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.27 - Classification loss over the synthetic training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp2/adversarial_adnd_G_noise/results_adversarial_bsG_COND_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_discr_loss.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.28 - Privacy loss over the synthetic training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp2/adversarial_adnd_G_noise/results_adversarial_bsG_COND_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_gen_ANN_loss.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.29 - Utility loss over the synthetic training and validation during the adversarial training.
    </figcaption>
</figure>
From this experiments we obtain a generator wich gives this new noisy distribution.
<figure>
    <img 
        src="/experiments/synthetic/exp2/adversarial_adnd_G_noise/c.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.30 - Noisy location distribution generated by or mechanism over the training and validation data.
    </figcaption>
    <img 
        src="/experiments/synthetic/exp2/adversarial_adnd_G_noise/c 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.31 - Noisy location distribution generated by or mechanism over the test data.
    </figcaption>
</figure>


Fig. 23: Experiment 2: adversarial training progress. In this experiment only, 
after the G network gets close the the optimal noise, the training curve starts oscillating. 
As future work we plan on repeating this experiment with further checks on the learning rate 
and the weight of the mutual information loss function to tackle the oscillation issue. 
So far we keep the model that retrieved the best noise function. For further details check the paper.

### Experiment 3 - Gowalla data
We move now to the Gowalla dataset. Following the learning curve plots for the third experiment in the paper.
We inject the laplacian noise according to what has been reported on the paper.
We obtain a new location distribution.
<figure>
    <img 
        src="/experiments/Gowalla/exp3/laplacian_noise/a.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.32 - Distribution of the Gowalla training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/laplacian_noise/a 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.33- Distribution of the Gowalla test data after the Laplacian noise injection.
    </figcaption>
</figure>

<figure>
    <img 
        src="/experiments/Gowalla/exp3/laplacian_noise/results_standalone_discriminator_yes_noise_bs_512_noe_150_Discriminator_loss_.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.34 - Classification loss over the Gowalla training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/laplacian_noise/results_standalone_discriminator_yes_noise_bs_512_noe_150_Discriminator_categ_acc_.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.35 - Classification accuracy over the Gowalla training and validation data after the Laplacian noise injection.
    </figcaption>
</figure>

The complete results are shown in the following table
<figure>
    <img 
        src="/experiments/Gowalla/exp3/laplacian_noise/lap_res.png" 
        alt="Loss class" height="69.5" width="254.25"
    >
    <figcaption>
        Fig.36 - Classification results over all the sets of Gowalla data after the Laplacian noise injection.
    </figcaption>
</figure>
We now train our adversarial paradigm.
<figure>
    <img 
        src="/experiments/Gowalla/exp3/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_categ_acc.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.37 - Classification accuracy over the Gowalla training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_loss.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.38 - Classification loss over the Gowalla training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_discr_loss.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.39 - Privacy loss over the Gowalla training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_gen_ANN_loss.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.40 - Utility loss over the Gowalla training and validation during the adversarial training.
    </figcaption>
</figure>
From this experiments we obtain a generator wich gives this new noisy distribution.
<figure>
    <img 
        src="/experiments/Gowalla/exp3/adversarial_and_G_noise/c.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.41 - Noisy location distribution generated by our mechanism over the training and validation data.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp3/adversarial_and_G_noise/c 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.42 - Noisy location distribution generated by our mechanism over the test data.
    </figcaption>
</figure>

### Experiment 4 - Gowalla data
Following the learning curve plots for the fourth experiment in the paper. We introduce a stricter constraint on utility.
We inject the laplacian noise according to what has been reported on the paper.
We obtain a new location distribution.
<figure>
    <img 
        src="/experiments/Gowalla/exp4/laplace_noise/a.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.43 - Distribution of the Gowalla training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp4/laplace_noise/a 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.44 - Distribution of the Gowalla test data after the Laplacian noise injection.
    </figcaption>
</figure>

<figure>
    <img 
        src="/experiments/Gowalla/exp4/laplace_noise/results_standalone_discriminator_yes_noise_bs_512_noe_100_Discriminator_loss_.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.45 - Classification loss over the Gowalla training and validation data after the Laplacian noise injection.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp4/laplace_noise/results_standalone_discriminator_yes_noise_bs_512_noe_100_Discriminator_categ_acc_.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.46 - Classification accuracy over the Gowalla training and validation data after the Laplacian noise injection.
    </figcaption>
</figure>

The complete results are shown in the following table
<figure>
    <img 
        src="/experiments/Gowalla/exp4/laplace_noise/res_lap.png" 
        alt="Loss class" height="69.5" width="254.25"
    >
    <figcaption>
        Fig.47 - Classification results over all the sets of synthetic data after the Laplacian noise injection.
    </figcaption>
</figure>
We now train our adversarial paradigm.
<figure>
    <img 
        src="/experiments/Gowalla/exp4/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_categ_acc.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.48 - Classification accuracy over the Gowalla training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp4/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_Discriminator_loss.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.49 - Classification loss over the Gowalla training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp4/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_discr_loss_right_iterations.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.50 - Privacy loss over the Gowalla training and validation during the adversarial training.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp4/adversarial_and_G_noise/results_adversarial_bsG_COND_DiffDist_512_noeG_30_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001_adv_gen_ANN_loss_right_iteration.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.51 - Utility loss over the Gowalla training and validation during the adversarial training.
    </figcaption>
</figure>
From this experiments we obtain a generator wich gives this new noisy distribution.
<figure>
    <img 
        src="/experiments/Gowalla/exp4/adversarial_and_G_noise/c.png" 
        alt="Loss class" height="360" width="480"
    >
    <figcaption>
        Fig.52 - Noisy location distribution generated by our mechanism over the training and validation data.
    </figcaption>
    <img 
        src="/experiments/Gowalla/exp4/adversarial_and_G_noise/c 2.png" 
        alt="is Acc class" height="360" width="480"
    >
    <figcaption>
        Fig.53 - Noisy location distribution generated by our mechanism over the test data.
    </figcaption>
</figure>
Experiment 4: adversarial training progress. So far we keep the model that retrieved 
the best noise function for both the Gowalla training and test data. In fact, 
in the iterations that follow the the chosen iteration, the mutual information 
loss does not significantly improve and it slightly oscillates. This often 
results in a better privacy on the training set but not necessarily on the test set.