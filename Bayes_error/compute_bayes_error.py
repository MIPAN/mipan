from keras.models import load_model
import tensorflow as tf
from keras import Model, Input
from keras.layers import Dense
import keras.backend as K
from keras import optimizers
import numpy as np
import random
import pickle
import os
from tqdm import tqdm
from tqdm import TqdmSynchronisationWarning
import warnings
import sys

####################################################  MI computation  ##################################################
def condition(i, number_of_elements, y_true, y_pred, M):
    return i < number_of_elements


def condition2(i, number_of_elements, Mx, Mx_tmp):
    return i < number_of_elements


def body(i, number_of_elements, y_true, y_pred, M):
    tmp = tf.matmul(tf.gather(y_pred, i, axis=0), tf.transpose(tf.gather(y_true, i, axis=0)))
    tmp = tf.reshape(tmp, shape=[1, tf.shape(y_true)[1], tf.shape(y_true)[1]])
    M = tf.concat([M, tmp], axis=0)
    return i + 1, number_of_elements, y_true, y_pred, M


def body2(i, number_of_elements, Mx, Mx_tmp):
    Mx = tf.concat([Mx, Mx_tmp], axis=0)
    return i + 1, number_of_elements, Mx, Mx_tmp


def condition_H_true(iter, H_true, P_marg_true):
    return iter < tf.shape(P_marg_true)[0]


def body_H_true(iter, H_true, P_marg_true):
    fix = tf.constant(0.0001)
    p_x = tf.gather(P_marg_true, iter, axis=0)
    a = tf.to_float(fix - (fix * p_x) + p_x)
    b = tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))
    H_true -= tf.to_float(a) * (tf.log(tf.to_float(a)) / b)
    return iter + 1, H_true, P_marg_true


def condition_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    return jter < tf.shape(P_marg_true)[0]


def condition_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    return kter < tf.shape(P_marg_pred)[0]


def body_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    kter = tf.constant(0)
    tmp = tf.constant([0.0])

    jter, kter, tmp, P_marg_pred, P_jk = tf.while_loop(condition_H_true_cond_pred_2,
                                                       body_H_true_cond_pred_2,
                                                       [jter, kter, tmp, P_marg_pred, P_jk])

    H_true_cond_pred += tmp
    return jter + 1, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk


def body_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    fix = tf.constant(0.0001)
    a = fix - (fix * P_jk[kter, jter]) + P_jk[kter, jter]
    b = fix - (fix * P_marg_pred[kter]) + P_marg_pred[kter]
    tmp += -(a * (tf.log(a / b) / tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))))
    return jter, kter + 1, tmp, P_marg_pred, P_jk


def MI_loss_G(y_true, y_pred):
    # return K.categorical_crossentropy(y_true, y_pred)

    rank = len(y_pred.shape)
    axis = -1 % rank
    y_pred = y_pred / tf.reduce_sum(y_pred, axis, True)

    y_true = tf.reshape(y_true, [tf.shape(y_true)[0], tf.shape(y_true)[1], 1])
    y_pred = tf.reshape(y_pred, [tf.shape(y_pred)[0], tf.shape(y_pred)[1], 1])

    # y_true = tf.Print(y_true, [y_true], message="y_true ---> ", summarize=10)
    number_of_elements = tf.shape(y_true)[0]
    number_of_elements = tf.Print(number_of_elements,
                                  [number_of_elements],
                                  message="number_of_elements ---> ", summarize=10)

    i = tf.constant(1)
    M = tf.matmul(tf.gather(y_pred, 0, axis=0), tf.transpose(tf.gather(y_true, 0, axis=0)))
    #M = tf.reshape(M, shape=[1, 4, 4])
    M = tf.reshape(M, shape=[1, 6, 6])

    i, number_of_elements, y_true, y_pred, M = tf.while_loop(
        cond=condition,
        body=body,
        loop_vars=[i, number_of_elements, y_true, y_pred, M],
        shape_invariants=[i.get_shape(),
                          number_of_elements.get_shape(),
                          y_true.get_shape(),
                          y_pred.get_shape(),
                          tf.TensorShape([None, M.get_shape().as_list()[1], M.get_shape().as_list()[2]])])
    # tf.TensorShape(None)])
    # tf.TensorShape([None, 4, 4])])

    F_jk = tf.reduce_sum(M, axis=0)
    P_jk = tf.to_float(F_jk) / tf.to_float(number_of_elements)

    P_marg_true = tf.to_float(tf.reduce_sum(y_true, axis=0)) / tf.to_float(number_of_elements)

    P_marg_pred = tf.to_float(tf.reduce_sum(y_pred, axis=0)) / tf.to_float(number_of_elements)

    iter = tf.constant(0)
    H_true = tf.constant([0.0])

    iter, H_true, P_marg_true = tf.while_loop(condition_H_true, body_H_true, [iter, H_true, P_marg_true])

    H_true = tf.Print(H_true, [H_true], message="H_true")

    H_true_cond_pred = tf.constant([0.0])
    jter = tf.constant(0)

    jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk = tf.while_loop(condition_H_true_cond_pred,
                                                                           body_H_true_cond_pred,
                                                                           [jter,
                                                                            H_true_cond_pred,
                                                                            P_marg_pred,
                                                                            P_marg_true,
                                                                            P_jk])

    H_true_cond_pred = tf.Print(H_true_cond_pred, [H_true_cond_pred], message="H_true_cond_pred")

    MI = H_true - H_true_cond_pred

    return MI


#   new loss function, sigmoid centered in 300 so that we can compare the distance we obtain, i.e. MSE, to this one
def adv_euclid_700_meters(y_true, y_pred):
    #   compute the mean squared error for the obtained value and supervision
    y_true = K.cast(y_true, dtype='float32')
    y_pred = K.cast(y_pred, dtype='float32')
    mse = tf.sqrt(tf.reduce_sum(tf.square(y_true - y_pred), reduction_indices=1))
    me = K.mean(mse)
    me = tf.scalar_mul(scalar=3250, x=me)
    me = me - 1000
    # me = tf.scalar_mul(scalar=0.1, x=me)
    me = tf.nn.softplus(me)
    #me = tf.square(me)
    return me

def build_classifier():
    hiddenLayersCard = 3
    hiddenNeuronsCard = [60, 100, 51, 66, 49, 65, 61, 49, 63, 100]
    # inputs of 2D vectors
    input_layer = Input(shape=(2,), name='discriminator_input')
    # at least one hidden layer
    hidden_layer = Dense(hiddenNeuronsCard[0], activation='relu', name='hidden_layer_0')(input_layer)
    # other hidden layers if any
    for idx in range(1, hiddenLayersCard):
        layer_name = 'hidden_layer_' + str(idx)
        hidden_layer = Dense(hiddenNeuronsCard[idx], activation='relu', name=layer_name)(hidden_layer)
    # output layer
    output_layer = Dense(4, activation='softmax', name='output_layer')(hidden_layer)

    #   create the network model
    discriminator = Model(inputs=input_layer, outputs=output_layer, name='discriminatorANN')

    # parallel_model = multi_gpu_model(discriminator, gpus=2)
    # parallel_model.compile(loss='categorical_crossentropy',
    #                       optimizer='adam',
    #                       metrics=['categorical_accuracy'])
    """
    discriminator.compile(loss=MI_loss,
                          optimizer='adam',
                          metrics=['categorical_accuracy'])
    """

    discriminator.compile(loss="categorical_crossentropy", optimizer='adam')

    # print(parallel_model.summary())
    # print discriminator.summary()

    return discriminator

def GPU_setup(id_GPU, memory_percentage):
    #   only use GPU with index id_GPU
    #   id_GPU is a string like "0"
    os.environ["CUDA_VISIBLE_DEVICES"] = id_GPU

    #   TensorFlow wizardry
    config = tf.ConfigProto()

    #   Don't pre_allocate memory; allocate as_needed
    config.gpu_options.allow_growth = True

    #   Only allow a total of half the GPU memory to be allocated
    #   memory_percentage is a float between 0 and 1
    config.gpu_options.per_process_gpu_memory_fraction = memory_percentage

    #   Create a session with the above options specified.
    K.tensorflow_backend.set_session(tf.Session(config=config))

def build_adv_model(generator_model_file, trained_weights_file=None):

    #   load the discriminator model
    #discriminatorLayer = load_model(filepath=discrimininator_model_file)
    discriminatorLayer = build_classifier()
    #   load the generator model
    generatorLayer = load_model(filepath=generator_model_file)

    ####################################################################
    ##############################  dNet  ##############################
    ####################################################################

    #   create the architecture for the discriminator where the the discriminator layer is active
    input_layer_discriminator = Input(shape=(2,), name='discriminator_Input')
    discriminatorLayer.trainable = True
    output_layer_discriminator = discriminatorLayer(input_layer_discriminator)

    #   create the discriminator model
    discriminator = Model(inputs=input_layer_discriminator, outputs=output_layer_discriminator,
                          name="discriminator")

    #   compile discriminator model
    """
    discriminator.compile(loss='categorical_crossentropy',
                          optimizer='adam',
                          metrics=['categorical_accuracy'])
    """
    discriminator.compile(loss="categorical_crossentropy",
                          optimizer='adam')

    trainable_weigths_discriminator_network = len(discriminator.trainable_weights)
    #print "trainable_weigths_discriminator_network ---> ", trainable_weigths_discriminator_network
    ####################################################################
    ##############################  advNet  ############################
    ####################################################################

    #   create an input layer for the advNet, it must have the same dimension as the gNet input
    input_layer_advNet = Input(shape=(4,), name='adversarial_Input')

    #   create the architecture for the adversarial net where the the discriminator model is not active and the
    #   generative layer is active
    discriminator.trainable = False
    generatorLayer.trainable = True

    trainable_weigths_generator_network = len(generatorLayer.trainable_weights)
    #print "trainable_weigths_generator_network ---> ", trainable_weigths_generator_network

    #   create advNet model architecture
    #   this model has one inout and two outputs
    #   two outputs are needed to check two loss functions
    #   the first output is the output of the generator layer
    #   the second outpu is the output of the discriminator module when it receives the generatorLayer output
    advNet = Model(input_layer_advNet, [generatorLayer(input_layer_advNet),
                                        discriminator(generatorLayer(input_layer_advNet))])

    #   we have two losses one is used to minize the mse between the input and the output of the generatorLayer while
    #   the other one is used to minize the opposite of the categorical_crossentropy so to maximize the distance between
    #   target and prediction for the classifier
    optimizer_adv = optimizers.Adam(lr=0.0001)
    alpha = 1.0#K.variable(float(sys.argv[5]))
    beta = 2.0#K.variable(float(sys.argv[6]))
    loss_weights = [alpha, beta]
    """
    advNet.compile(loss=[adv_euclid_700_meters, opposite_categorical_crossentropy],
                   loss_weights=loss_weights,
                   optimizer='adam',
                   metrics=['categorical_accuracy'])
    """
    advNet.compile(loss=[adv_euclid_700_meters, MI_loss_G],
                   loss_weights=loss_weights,
                   optimizer='adam')

    trainable_weigths_adversarial_network = len(advNet.trainable_weights)
    #print "trainable_weigths_adversarial_network ---> ", trainable_weigths_adversarial_network

    #print discriminator.summary()
    #print advNet.summary()

    #print "trainable_weigths_discriminator_network ---> ", trainable_weigths_discriminator_network
    #print "trainable_weigths_generator_network ---> ", trainable_weigths_generator_network
    #print "trainable_weigths_adversarial_network ---> ", trainable_weigths_adversarial_network

    if trained_weights_file is not None:
        advNet.load_weights(filepath=trained_weights_file)
        #print "weights loaded"
    return advNet

def loadDataset(inputFile):
    with open(inputFile) as f:
        #print bcolors.OKGREEN + "Loading file..." + bcolors.ENDC
        loaded_obj = pickle.load(f)
        #print bcolors.OKDARKTHEME + str(loaded_obj.dtypes) + bcolors.ENDC
        #print bcolors.OKDARKTHEME + "(rows, cols) = " + str(loaded_obj.shape) + bcolors.ENDC
        return loaded_obj
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################
########################################################################################################################

def compute_Bayes_error(block_side_length_meters, repetitions, filepath, gpu_id, gpu_perc):
    os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
    GPU_setup(str(gpu_id), gpu_perc)

    #   number of squares per row or col
    #constant = 6500.0/50.0
    constant = 6500.0/float(block_side_length_meters)

    #   spacing to get the tassellation
    div_const = 2.0/constant

    #   load generator model
    generator_laplace = load_model(filepath="path_to"
                                            "standalone_saved_models/generator_3hl_100x100x100")


    #   create adversarial model for getting outputs from our generator
    our_generator = build_adv_model(generator_model_file='path_to/generator_3hl_100x100x100',
                                    trained_weights_file='path_to/'
                                                         'new_iter_new_val/'
                                                         'results_adversarial_bsG_COND_128_noeG_100_bsC_512_noeC_3000_alpha_1_beta_2_learning_rate_0.0001/'
                                                         'adv_net_model_WEIGHTS_after_adversarial_training149')

    #print generator_laplace.summary()
    #print our_generator.summary()

    #   load the dataset
    loaded_data = loadDataset(inputFile=filepath).values

    print loaded_data.shape
    
    #   list of unique class identifiers sorted in increasing order
    classes = np.unique(loaded_data[:, 6])

    #   substitute class ids with 0, 1, 2, ...
    for iterator_ in range(0, len(classes)):
        idx_tmp = np.where(loaded_data[:, 6] == classes[iterator_])
        #print "old class ", classes[iterator_], " ---> new class ", iterator_
        for idx_ in idx_tmp:
            loaded_data[idx_, 6] = iterator_

    #print classes

    #   it does not contain the locations but the indexes of the locations
    selected_points_idx = []
    #   randomly extract one record for each class
    for current_class in range(0, len(classes)):
        idx_list = np.where(loaded_data[:, 6] == current_class)[0]
        random.shuffle(idx_list)
	for k in range(0, len(idx_list)):
            selected_points_idx.append(idx_list[k])
        #print loaded_data[idx_list[0], :]

    #print div_const

    #   number of obfuscated positions to obtain
    #num_points = 200
    num_points = repetitions
    #   matrix to contain the data for the generator and the classes
    #mat_input = np.zeros((len(np.unique(loaded_data[:, 6])) * num_points, 5))

    mat_input = np.zeros((len(selected_points_idx) * num_points, 5))

    for i in range(0, mat_input.shape[0]):
        #   current id updated the class label ever num_points records
        current_idx = selected_points_idx[i//num_points]
        #   always pick the one point related to the current class
        current_location = loaded_data[current_idx, 0:2]
        #   get the id of the user
        current_user_id = loaded_data[current_idx, 6]

        mat_input[i, 0] = current_location[0]
        mat_input[i, 1] = current_location[1]
        #   put the seeds
	seed1 = random.random()
	while seed1 in mat_input[:, 2]:
	    seed1 = random.random()
        mat_input[i, 2] = seed1
	seed2 = random.random()
	while seed2 in mat_input[:, 3]:
	    seed2 = random.random()
        mat_input[i, 3] = seed2
        mat_input[i, 4] = current_user_id

    print "Number of elements ---> ", mat_input.shape[0]
    print "Number of unique elements ---> ", len(np.unique(mat_input[:, 2:4], axis=0))

    #   create points with laplacian noise
    res_laplace = generator_laplace.predict(x=mat_input[:, 0:4], batch_size=mat_input.shape[0])
    #print res_laplace

    #   create points with our noise
    res_our = our_generator.predict(x=mat_input[:, 0:4], batch_size=mat_input.shape[0])[0]
    #print res_our

    #   add one so we can have coordinated from 0 to 2
    for i in range(0, res_laplace.shape[0]):
        res_laplace[i, 0] += 1
        res_laplace[i, 1] += 1

        res_our[i, 0] += 1
        res_our[i, 1] += 1

    #print res_laplace
    #print res_our

    #   for each coordinate retrieve the corresponding bloch ---> DISCRETIZATION
    #block_class_laplace = np.zeros((len(np.unique(loaded_data[:, 6])) * num_points, 2), dtype=np.int64)

    block_class_laplace = np.zeros((len(selected_points_idx) * num_points, 2), dtype=np.int64)

    for i in range(0, mat_input.shape[0]):
        #if res_laplace[i, 0] >= 2.0 or res_laplace[i, 1] >= 2.0:
        #    sys.exit("ERROR DIMENSION")
        if res_laplace[i, 0]//div_const > constant - 1 or res_laplace[i, 1]//div_const > constant - 1:
	    sys.exit("ERROR DIMENSION")
        elif res_laplace[i, 0] // div_const < 0 or res_laplace[i, 1] // div_const < 0:
            sys.exit("ERROR DIMENSION")
        else:
            block_id = (res_laplace[i, 0]//div_const) + ((res_laplace[i, 1] // div_const) * constant)

            block_class_laplace[i, 0] = int(block_id)
            block_class_laplace[i, 1] = int(mat_input[i, 4])

    #print block_class_laplace

    #block_class_our = np.zeros((len(np.unique(loaded_data[:, 6])) * num_points, 2), dtype=np.int64)
    block_class_our = np.zeros((len(selected_points_idx) * num_points, 2), dtype=np.int64)

    for i in range(0, mat_input.shape[0]):
        #if res_our[i, 0] >= 2.0 or res_our[i, 1] >= 2.0:
            #sys.exit("ERROR DIMENSION")
        if res_our[i, 0]//div_const > constant - 1 or res_our[i, 1]//div_const > constant - 1:
            sys.exit("ERROR DIMENSION")
        elif res_our[i, 0] // div_const < 0 or res_our[i, 1] // div_const < 0:
            sys.exit("ERROR DIMENSION")
        else:
            block_id = (res_our[i, 0]//div_const) + ((res_our[i, 1] // div_const) * constant)

            block_class_our[i, 0] = int(block_id)
            block_class_our[i, 1] = int(mat_input[i, 4])

    ################################################
    #   create a list with one entry for each block; each blok is a list with the number of elements for each class
    #   empty blocks are not considered
    list_block_laplace = []
    #print block_class_laplace

    for block_id in np.unique(block_class_laplace[:, 0]):

        list_tmp = []

        for class_ in np.unique(block_class_laplace[:, 1]):
            list_tmp.append(len(np.where((block_class_laplace[:, 1] == class_) &
                                         (block_class_laplace[:, 0] == block_id))[0]))

        list_block_laplace.append(list_tmp)

    list_block_our = []
    #print block_class_our

    for block_id in np.unique(block_class_our[:, 0]):

        list_tmp = []

        for class_ in np.unique(block_class_our[:, 1]):
            list_tmp.append(len(np.where((block_class_our[:, 1] == class_) &
                                         (block_class_our[:, 0] == block_id))[0]))

        list_block_our.append(list_tmp)

    ########################################################

    acc_laplace = 0
    for block in list_block_laplace:
        acc_laplace += max(block)/float(mat_input.shape[0])

    bayes_error_laplace = 1.0 - acc_laplace

    tqdm.write("bayes_error_laplace ---> " + str(bayes_error_laplace))

    acc_our = 0
    for block in list_block_our:
        acc_our += max(block) / float(mat_input.shape[0])

    bayes_error_our = 1.0 - acc_our

    tqdm.write("bayes_error_our ---> " + str(bayes_error_our))
    K.clear_session()
    if (bayes_error_our < bayes_error_laplace):
        return False
    else:
        return True

if __name__ == "__main__":
    res = compute_Bayes_error(block_side_length_meters=float(sys.argv[1]),
                              path_to_data_df=sys.argv[2],
                              gpu_id=sys.argv[3],
                              gpu_percentage=float(sys.argv[4]),
                              repetitions=int((sys.argv[5])))
    print "Bayes error ---> ", res