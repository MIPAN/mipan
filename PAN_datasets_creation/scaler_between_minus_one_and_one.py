"""
when a column is passed it scales between -1 and 1 because the passed min and max are obtained from the set limited
areas
"""


def scaler_between_minus_one_and_one(column, min_column, max_column):
    #   column is a column vector
    for i in range(0, len(column)):
        column[i] = 2 * ((column[i] - min_column) / float(max_column - min_column)) - 1

    return column
