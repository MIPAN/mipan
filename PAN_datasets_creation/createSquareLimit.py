"""
given a central position and side length in meters find the coordinates of the verices to delimit the area of interest
"""

from position import *
from addLatLongNoise import *


def create_square_limit(central_position, side_length):
    #   half side length forced to be float
    half_side_length = side_length / float(2)

    #############################################  north_western limit  ################################################
    #   go to the north
    nw_tmp = addVectorToPos(pos=central_position, distance=half_side_length, angle=0)

    #   go to the west
    pos_tmp = position(nw_tmp[0], nw_tmp[1])
    nw_tmp = addVectorToPos(pos=pos_tmp, distance=half_side_length, angle=-math.pi / 2)

    #   final position
    nw = position(nw_tmp[0], nw_tmp[1])

    #############################################  north_eastern limit  ################################################
    #   go to the north
    ne_tmp = addVectorToPos(pos=central_position, distance=half_side_length, angle=0)

    #   go to the east
    pos_tmp = position(ne_tmp[0], ne_tmp[1])
    ne_tmp = addVectorToPos(pos=pos_tmp, distance=half_side_length, angle=math.pi / 2)

    #   final position
    ne = position(ne_tmp[0], ne_tmp[1])

    #############################################  south_western limit  ################################################
    #   go to the south
    sw_tmp = addVectorToPos(pos=central_position, distance=half_side_length, angle=math.pi)

    #   go to the west
    pos_tmp = position(sw_tmp[0], sw_tmp[1])
    sw_tmp = addVectorToPos(pos=pos_tmp, distance=half_side_length, angle=-math.pi / 2)

    #   final position
    sw = position(sw_tmp[0], sw_tmp[1])

    #############################################  south_western limit  ################################################
    #   go to the south
    se_tmp = addVectorToPos(pos=central_position, distance=half_side_length, angle=math.pi)

    #   go to the east
    pos_tmp = position(se_tmp[0], se_tmp[1])
    se_tmp = addVectorToPos(pos=pos_tmp, distance=half_side_length, angle=math.pi / 2)

    #   final position
    se = position(se_tmp[0], se_tmp[1])

    #   return the limits
    min_lat = se.latitude
    max_lat = nw.latitude

    min_longit = nw.longitude
    max_longit = ne.longitude

    print "nw.latitude ", nw.latitude
    print "nw.longitude ", nw.longitude

    print "ne.latitude ", ne.latitude
    print "ne.longitude ", ne.longitude

    print "sw.latitude ", sw.latitude
    print "sw.longitude ", sw.longitude

    print "se.latitude ", se.latitude
    print "se.longitude ", se.longitude

    print "min_lat ", min_lat
    print "min_lon ", min_longit

    print "max_lat ", max_lat
    print "max_lon ", max_longit

    return [min_lat, max_lat, min_longit, max_longit]
