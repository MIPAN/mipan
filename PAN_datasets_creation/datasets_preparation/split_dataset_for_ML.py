import numpy as np
import pandas as pn
import pickle

def datasetSplit(datasetToSplit, testPercentage):
    #   convert df to matrix for dataseToSplit
    datasetToSplit_matrix = datasetToSplit.values
    # print datasetToSplit_matrix

    #   compute number of rows and cols for the dataseToSplit
    numRow_datasetToSplit_matrix = datasetToSplit_matrix.shape[0]
    # print "numRow_datasetToSplit_matrix ---> ", numRow_datasetToSplit_matrix
    numCol_datasetToSplit_matrix = datasetToSplit_matrix.shape[1]
    # print "numCol_datasetToSplit_matrix ---> ", numCol_datasetToSplit_matrix

    #   prepare list of list for training set
    trainingSet_list = []

    #   prepare list of list for test set
    testSet_list = []

    #   list of unique user ids
    unique_user_ids_list = np.unique(ar=datasetToSplit_matrix[:, 0])
    print "unique_user_ids_list ---> ", unique_user_ids_list

    #   loop over ids
    for i in unique_user_ids_list:
        #   check in which positions every id is: eaning the index in the dataset matrix
        where_id_in_area_of_interest_most_freq_users_original_dataset_df = \
            np.where(datasetToSplit_matrix[:, 0] == i)[0]
        #   shuffle the ids order
        np.random.shuffle(where_id_in_area_of_interest_most_freq_users_original_dataset_df)
        # print where_id_in_area_of_interest_most_freq_users_original_dataset_df
        #   split into training and test set

        #   compute number of elements in each set
        testCard_current_id = testPercentage * \
                              len(where_id_in_area_of_interest_most_freq_users_original_dataset_df) // 100
        print "testCard ---> ", testCard_current_id
        trainCard_current_id = len(where_id_in_area_of_interest_most_freq_users_original_dataset_df) - \
                               testCard_current_id
        # print "trainCard ---> ", trainCard

        #   push elements in the matrices of training and test set
        for j in range(0, trainCard_current_id):
            trainingSet_list.append(
                datasetToSplit_matrix[where_id_in_area_of_interest_most_freq_users_original_dataset_df[j]])

        for k in range(trainCard_current_id, len(where_id_in_area_of_interest_most_freq_users_original_dataset_df)):
            testSet_list.append(
                datasetToSplit_matrix[where_id_in_area_of_interest_most_freq_users_original_dataset_df[k]])

    # print "np.array(trainingSet_list)\n", np.array(trainingSet_list)
    # print "np.array(testSet_list)\n", np.array(testSet_list)

    # print len(np.where(np.array(trainingSet_list)[:, 0] == datasetToSplit_matrix[736, 0])[0])
    # print len(np.where(np.array(testSet_list)[:, 0] == datasetToSplit_matrix[736, 0])[0])

    trainingSet_no_noise_df = pn.DataFrame(data=np.array(trainingSet_list), columns=datasetToSplit.columns.values)
    testSet_no_noise_df = pn.DataFrame(data=np.array(testSet_list), columns=datasetToSplit.columns.values)

    print "trainingSet_no_noise_df", trainingSet_no_noise_df
    print "testSet_no_noise_df", testSet_no_noise_df
    """
    with open("/home/comete/mromanel/PAN_datasets_2/training_set_original_data_no_scaling_no_noise", 'wb') as f:
        pickle.dump(trainingSet_no_noise_df, f)

    with open("/home/comete/mromanel/PAN_datasets_2/validation_set_original_data_no_scaling_no_noise", 'wb') as f:
        pickle.dump(testSet_no_noise_df, f)
    """
    return [trainingSet_no_noise_df, testSet_no_noise_df]
