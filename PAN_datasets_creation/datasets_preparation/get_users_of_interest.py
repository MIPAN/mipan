from loadDataset import *
from createSquareLimit import *
import numpy as np
import pandas as pn
import operator


###########################################  get elements in square of interest  #######################################

def getElementsInSquareOfInterest(inputDataset, central_position, side_length):
    #   load dataframe from .pickle file
    loadedObj = loadDataset(inputFile=inputDataset)

    #   dataframe to matrix
    matrix_dataset = loadedObj.values

    # print matrix_dataset

    #   number of rows in matrix_dataset
    numRows_matrix_dataset = matrix_dataset.shape[0]

    #   number of columns in matrix_dataset
    numCols_matrix_dataset = matrix_dataset.shape[1]

    #   get the limits
    limited_area = create_square_limit(central_position=central_position, side_length=side_length)

    #   limits for not noisy data
    min_lat = limited_area[0]
    max_lat = limited_area[1]
    min_lon = limited_area[2]
    max_lon = limited_area[3]
    # print "min lat is: ", min_lat
    # print "min lon is: ", min_lon
    # print "max lat is: ", max_lat
    # print "max lon is: ", max_lon

    """ old way to remove elements not in the square
    #   we are going to keep only the data in matrix_dataset that are inside the limits
    #   find the indexes of the lines where the conditions are not respected
    #   find elements whose lat is less than min_lat: the numbers are row indexes
    drop0 = np.where(matrix_dataset[:, 1] < min_lat)
    #   find elements whose lat is less than max_lat
    drop1 = np.where(matrix_dataset[:, 1] > max_lat)
    #   find elements whose lon is less than min_lon
    drop2 = np.where(matrix_dataset[:, 2] < min_lon)
    #   find elements whose lon is less than max_lon
    drop3 = np.where(matrix_dataset[:, 2] > max_lon)
    # print drop0
    # print drop1
    # print drop2
    # print drop3

    #   check how many elements (with repetitions) should be discarded: the numbers are row indexes
    sum_of_drop_sets = len(drop0[0]) + len(drop1[0]) + len(drop2[0]) + len(drop3[0])
    # print sum_of_drop_sets

    #   create empty list to store the unique indexes of the elements to be discarded
    list_ = []

    #   extend list
    list_.extend(drop0[0])
    list_.extend(drop1[0])
    list_.extend(drop2[0])
    list_.extend(drop3[0])
    # print len(list_)
    # print list_[0], list_[len(list_)-1]

    #   create a set from list_
    set_list_ = set(list_)
    # print len(set_list_)
    """
    """ new way to remove elements not in the square """
    #   we are going to keep only the data in matrix_dataset that are inside the limits
    #   find the indexes of the lines where the conditions are not respected
    #   find elements whose lat is less than min_lat: the numbers are row indexes
    elements_to_drop = np.where((matrix_dataset[:, 1] < min_lat) |
                                (matrix_dataset[:, 1] > max_lat) |
                                (matrix_dataset[:, 2] < min_lon) |
                                (matrix_dataset[:, 2] > max_lon))[0]

    #   create empty list to store the unique indexes of the elements to be discarded
    list_ = []
    #   extend list
    list_.extend(elements_to_drop)
    # print list_
    # print "len(list_) ---> ", len(list_)
    #   create a set from list_
    set_list_ = set(list_)
    # print "len(set_list_) ---> ", len(set_list_)

    #   set with all indexes in matrix_dataset
    list_tmp = []
    for i in range(0, numRows_matrix_dataset):
        list_tmp.append(i)
    set_all = set(list_tmp)
    # print len(set_all)

    #   compute difference in terms of elements between set_all and set_list_: the numbers are row indexes
    set_diff = set_all.difference(set_list_)
    print "set_diff\n", set_diff

    #   create a new matrix_dataset where the number of rows is the number of elements not to be discarded from the
    #   previus one
    numRows_new_matrix_dataset = len(set_diff)

    new_matrix_dataset = np.zeros((numRows_new_matrix_dataset, numCols_matrix_dataset))

    #   index for the rows in new_matrix_dataset
    row_in_new_matrix_dataset = 0

    #   insert in new_matrix_dataset the elements which have not been discarded
    for i in set_diff:
        new_matrix_dataset[row_in_new_matrix_dataset, :] = matrix_dataset[i, :]
        row_in_new_matrix_dataset += 1

    #   number of rows in new_matrix_dataset
    new_matrix_dataset_rows = new_matrix_dataset.shape[0]
    #   number of cols in new_matrix_dataset
    new_matrix_dataset_cols = new_matrix_dataset.shape[1]
    # print len(np.where(new_matrix_dataset[:, 0] == 1.0))
    # print max(new_matrix_dataset[:, 0])
    # print new_matrix_dataset_rows
    # print new_matrix_dataset_cols

    #   check whether there are problems or not
    # for el in new_matrix_dataset[:, 1]:
    #    if el < min_lat or el > max_lat:
    #       sys.exit("Some elements are not in the lat range")
    # for el in new_matrix_dataset[:, 2]:
    #    if el < min_lon or el > max_lon:
    #       sys.exit("Some elements are not in the lon range")

    #   check number of classes and number of samples in new_matrix_dataset
    number_of_classes = len(np.unique(new_matrix_dataset[:, 0]))
    number_of_samples = new_matrix_dataset_rows
    # print "\nNumber of classes in the square of interest: ", number_of_classes
    # print "\nNumber of samples in the square of interest: ", number_of_samples

    print "new_matrix_dataset\n", new_matrix_dataset

    return [new_matrix_dataset, loadedObj.columns.values]


################################  get n most frequent elements in square of interest  ##################################

def getNMostFrequentElements(dataset, usersOI_card, colNames):
    #   unique over the userID aka classes column with occurrences counting
    unique, counts = np.unique(dataset[:, 0], return_counts=True)
    #   transform the result of the previous operation in a python dict so we can sort it
    #   zip associates id with number of occurences
    #   then a dict gets built
    dict_ = dict(zip(unique, counts))
    # print dict_
    #   sorting the value of unique in order of increasing occurences
    sorted_x = sorted(dict_.items(), key=operator.itemgetter(1), )
    # print sorted_x

    #   empty vector to store userID of interest
    usersOI = []
    #   select userID of interest and push them back in the usersOI list
    for i in range(len(sorted_x) - 1, len(sorted_x) - usersOI_card - 1, -1):
        # print "(userID, occurences) ---> ", sorted_x[i]
        usersOI.append(int(sorted_x[i][0]))
    print "usersOI ---> ", usersOI

    #   store new_matrix_dataset in a dataframe to be saved and reused: this dataframe contains the positions of
    #   interest for the current project
    area_of_interest_most_freq_users_original_dataset_df = pn.DataFrame(data=dataset, columns=colNames)

    #   restrict the dataframe to the users of interest
    area_of_interest_most_freq_users_original_dataset_df = \
        area_of_interest_most_freq_users_original_dataset_df.loc[
            area_of_interest_most_freq_users_original_dataset_df['userID'].isin(usersOI)]

    #   because of the initial conversion from dataframe to matrix: the dtype will be a lower-common-denominator
    #   dtype (implicit upcasting); that is to say if the dtypes (even of numeric types) are mixed,
    #   the one that accommodates all will be chosen. Use this with care if you are not dealing with the blocks;
    #   so forcing type in user_id
    area_of_interest_most_freq_users_original_dataset_df['userID'] = \
        area_of_interest_most_freq_users_original_dataset_df['userID'].astype(np.int64)
    # print "\n"
    # print len(np.where(area_of_interest_most_freq_users_original_dataset_df.values[:, 0] == 1671.0)[0])
    # print area_of_interest_most_freq_users_original_dataset_df

    return area_of_interest_most_freq_users_original_dataset_df


def get_users_of_interest(inputDataset, central_position, side_length, usersOI_card,
                          users_of_interest_most_frequent_check_in_no_scaling_original_positions_df):
    dataset, colNames = getElementsInSquareOfInterest(inputDataset=inputDataset,
                                                      central_position=central_position,
                                                      side_length=side_length)

    n_mostFrequentElements = \
        getNMostFrequentElements(dataset=dataset, usersOI_card=usersOI_card, colNames=colNames)
    #   save area_of_interest_most_freq_users_original_dataset_df

    n_mostFrequentElements_numRows = n_mostFrequentElements.shape[0]

    #   reset index otherwise concat has issues recognising tow indexes
    n_mostFrequentElements = n_mostFrequentElements.reset_index(drop=True)


    position_id_list = []
    for i in range(0, n_mostFrequentElements_numRows):
        position_id_list.append(i)

    position_id_df = pn.DataFrame(data=position_id_list, columns=["position_id"])

    position_id_df = position_id_df.reset_index(drop=True)

    n_mostFrequentElements = pn.concat(objs=[n_mostFrequentElements, position_id_df], axis=1)

    print n_mostFrequentElements

    with open(users_of_interest_most_frequent_check_in_no_scaling_original_positions_df, 'wb') as f:
        pickle.dump(n_mostFrequentElements, f)

    return n_mostFrequentElements
