"""
this function takes a dataset from a .txt file, transforms it into a pandas dataFrame and saves it as .pickle file that
can be loaded
"""
from bcolors import *
import pandas as pn
import pickle
import numpy as np


def createDataset_fromTXT(inputFile, original_colNames, save_df_file, colsToDrop=None):
    print bcolors.OKGREEN + "Open file ---> " + inputFile + bcolors.ENDC

    #   open inputFile as readOnly
    # fileVar = open(inputFile, 'r')

    # l = [map(str, line.split('\t')) for line in f]

    #   read file into pandas dataFrame: the file gets read as a table and gets stored
    print bcolors.OKGREEN + "Converting..." + bcolors.ENDC
    df = pn.read_table(inputFile, names=original_colNames, delim_whitespace=True)
    df = pn.DataFrame(df)

    print bcolors.OKDARKTHEME + \
          "\nThe datasets extracted from " + inputFile + " has the following datatypes: " + bcolors.ENDC
    print df.dtypes

    # get number or rows and columns
    (df_rows, df_columns) = df.shape
    print bcolors.OKDARKTHEME + \
          "\nThe datasets extracted from " + inputFile + " has " + str(df_rows) + " rows and " + str(df_columns) + \
          " columns." + bcolors.ENDC

    # print numpy.asarray(df.iloc[0])

    #   drop the columns we are not interested in
    if colsToDrop is not None:
        df = df.drop(columns=colsToDrop)

    # get number or rows and columns
    (df_rows, df_columns) = df.shape
    print bcolors.OKDARKTHEME + \
          "\nThe following columns have been dropped: " + bcolors.ENDC
    print colsToDrop
    print bcolors.OKDARKTHEME + "The datasets extracted from " + inputFile + " has " + str(df_rows) + \
          " rows and " + str(df_columns) + " columns." + bcolors.ENDC
    print bcolors.OKDARKTHEME + \
          "\nThe current columns are: " + bcolors.ENDC
    print df.columns.values
    print bcolors.OKDARKTHEME + \
          "\nThe current datasets extracted from " + inputFile + " has the following datatypes: " + bcolors.ENDC
    print df.dtypes

    #   convert data type in columns
    # df['userID'] = df['userID'].astype(np.int32)
    # df['latitude'] = df['latitude'].astype(np.float32)
    # df['longitude'] = df['longitude'].astype(np.float32)

    print bcolors.OKGREEN + "Saving file..." + bcolors.ENDC

    #   save dataFrame in .pickle file
    with open(save_df_file, 'wb') as f:
        pickle.dump(df, f)
