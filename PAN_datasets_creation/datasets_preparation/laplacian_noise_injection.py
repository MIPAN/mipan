import numpy as np
import position
from addLatLongNoise import *
from createSquareLimit import *


def noiseInjection(trainingSet_no_noise_df, testSet_no_noise_df,
                   repetitions, epsilon, side_length, noisy_side_length, central_position,
                   validationSet_no_noise_df=None):
    #   get the noisy limits
    colNames = trainingSet_no_noise_df.columns.values
    if noisy_side_length < side_length:
        sys.exit("ERROR: noise_side_length < side_length")
    else:
        noise_limited_area = create_square_limit(central_position=central_position, side_length=noisy_side_length)
    #   limits for noisy data
    noisy_min_lat = noise_limited_area[0]
    noisy_max_lat = noise_limited_area[1]
    noisy_min_lon = noise_limited_area[2]
    noisy_max_lon = noise_limited_area[3]

    #   computing numeber of cols and rows training set
    trainingSet_no_noise_mat = trainingSet_no_noise_df.values
    nRow_trainingSet_no_noise_df, nCol_trainingSet_no_noise_df = \
        trainingSet_no_noise_mat.shape

    if validationSet_no_noise_df is not None:
        #   computing numeber of cols and rows validation set
        validationSet_no_noise_mat = validationSet_no_noise_df.values
        nRow_validationSet_no_noise_df, nCol_validationSet_no_noise_df = \
            validationSet_no_noise_mat.shape

    #   computing numeber of cols and rows test set
    testSet_no_noise_mat = testSet_no_noise_df.values
    nRow_testSet_no_noise_df, nCol_testSet_no_noise_df = \
        testSet_no_noise_mat.shape

    #   create new matrix for training set with noise
    noisy_training_set_mat = np.zeros((nRow_trainingSet_no_noise_df * repetitions, nCol_trainingSet_no_noise_df))

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    #   create new matrix for adversarial training set with noise
    adv_training_set_mat = np.zeros((nRow_trainingSet_no_noise_df * repetitions, 8))
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

    #   loop over elements in trainingSet_no_noise_df
    for i in range(0, nRow_trainingSet_no_noise_df):
        #   loop over repetitions
        for j in range(0, repetitions):
            #   index for element in noisy_training_set_mat
            idx = i * repetitions + j
            #   create a position with current lat and lon
            pos_tmp = position(trainingSet_no_noise_mat[i, 1], trainingSet_no_noise_mat[i, 2])
            #   obtain a noisy position
            noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
            noisy_pos = noise[0]
            """
            #   check whether the noisy position is within the limits
            while noisy_pos[0] < noisy_min_lat or \
                    noisy_pos[0] > noisy_max_lat or \
                    noisy_pos[1] < noisy_min_lon or \
                    noisy_pos[1] > noisy_max_lon:
                noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
                noisy_pos = noise[0]
            """
            if noisy_pos[0] < noisy_min_lat:
                noisy_pos[0] = noisy_min_lat
            elif noisy_pos[0] > noisy_max_lat:
                noisy_pos[0] = noisy_max_lat

            if noisy_pos[1] < noisy_min_lon:
                noisy_pos[1] = noisy_min_lon
            if noisy_pos[1] > noisy_max_lon:
                noisy_pos[1] = noisy_max_lon
            #   update final_matrix_dataset with a new sample
            noisy_training_set_mat[idx, 0] = trainingSet_no_noise_mat[i, 0]
            noisy_training_set_mat[idx, 1] = noisy_pos[0]
            noisy_training_set_mat[idx, 2] = noisy_pos[1]
            noisy_training_set_mat[idx, 3] = trainingSet_no_noise_mat[i, 3]
            # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
            #   update matrix for adversarial training set with noise
            adv_training_set_mat[idx, 0] = pos_tmp.latitude
            adv_training_set_mat[idx, 1] = pos_tmp.longitude
            adv_training_set_mat[idx, 2] = noise[3]
            adv_training_set_mat[idx, 3] = noise[2]
            adv_training_set_mat[idx, 4] = noisy_pos[0]
            adv_training_set_mat[idx, 5] = noisy_pos[1]
            adv_training_set_mat[idx, 6] = trainingSet_no_noise_mat[i, 0]
            adv_training_set_mat[idx, 7] = trainingSet_no_noise_mat[i, 3]
            # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    #   check whether there are problems or not
    for i in range(0, noisy_training_set_mat.shape[0]):
        if noisy_training_set_mat[i, 1] < noisy_min_lat or noisy_training_set_mat[i, 1] > noisy_max_lat:
            sys.exit("Some elements are not in the noisy lat range")
        if noisy_training_set_mat[i, 2] < noisy_min_lon or noisy_training_set_mat[i, 2] > noisy_max_lon:
            sys.exit("Some elements are not in the noisy lon range")
    # print min(noisy_training_set_mat[:, 0])
    # print max(noisy_training_set_mat[:, 0])
    # print min(noisy_training_set_mat[:, 1])
    # print max(noisy_training_set_mat[:, 1])
    # print min(noisy_training_set_mat[:, 2])
    # print max(noisy_training_set_mat[:, 2])

    #   create new matrix for test set with noise
    noisy_test_set_mat = np.zeros((nRow_testSet_no_noise_df * repetitions, nCol_testSet_no_noise_df))
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    #   create new matrix for adversarial test set with noise
    adv_test_set_mat = np.zeros((nRow_testSet_no_noise_df * repetitions, 8))
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    #   loop over elements in new_matrix_dataset
    for i in range(0, nRow_testSet_no_noise_df):
        #   loop over repetitions
        for j in range(0, repetitions):
            #   index for element in final_matrix_dataset
            idx = i * repetitions + j
            #   create a position with current lat and lon
            pos_tmp = position(testSet_no_noise_mat[i, 1], testSet_no_noise_mat[i, 2])
            #   obtain a noisy position
            noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
            noisy_pos = noise[0]
            """
            #   check whether the noisy position is within the limits
            while noisy_pos[0] < noisy_min_lat or \
                    noisy_pos[0] > noisy_max_lat or \
                    noisy_pos[1] < noisy_min_lon or \
                    noisy_pos[1] > noisy_max_lon:
                noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
                noisy_pos = noise[0]
            """
            if noisy_pos[0] < noisy_min_lat:
                noisy_pos[0] = noisy_min_lat
            elif noisy_pos[0] > noisy_max_lat:
                noisy_pos[0] = noisy_max_lat

            if noisy_pos[1] < noisy_min_lon:
                noisy_pos[1] = noisy_min_lon
            if noisy_pos[1] > noisy_max_lon:
                noisy_pos[1] = noisy_max_lon
            #   update final_matrix_dataset with a new sample
            noisy_test_set_mat[idx, 0] = testSet_no_noise_mat[i, 0]
            noisy_test_set_mat[idx, 1] = noisy_pos[0]
            noisy_test_set_mat[idx, 2] = noisy_pos[1]
            noisy_test_set_mat[idx, 3] = testSet_no_noise_mat[i, 3]
            # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
            #   update matrix for adversarial test set with noise
            adv_test_set_mat[idx, 0] = pos_tmp.latitude
            adv_test_set_mat[idx, 1] = pos_tmp.longitude
            adv_test_set_mat[idx, 2] = noise[3]
            adv_test_set_mat[idx, 3] = noise[2]
            adv_test_set_mat[idx, 4] = noisy_pos[0]
            adv_test_set_mat[idx, 5] = noisy_pos[1]
            adv_test_set_mat[idx, 6] = testSet_no_noise_mat[i, 0]
            adv_test_set_mat[idx, 7] = testSet_no_noise_mat[i, 3]
            # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

    #   check whether there are problems or not
    for i in range(0, noisy_test_set_mat.shape[0]):
        if noisy_test_set_mat[i, 1] < noisy_min_lat or noisy_test_set_mat[i, 1] > noisy_max_lat:
            sys.exit("Some elements are not in the noisy lat range")
        if noisy_test_set_mat[i, 2] < noisy_min_lon or noisy_test_set_mat[i, 2] > noisy_max_lon:
            sys.exit("Some elements are not in the noisy lon range")

    if validationSet_no_noise_df is not None:
        #   create new matrix for validation set with noise
        noisy_validation_set_mat = np.zeros((nRow_validationSet_no_noise_df * repetitions, nCol_validationSet_no_noise_df))
        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
        #   create new matrix for adversarial validation set with noise
        adv_validation_set_mat = np.zeros((nRow_validationSet_no_noise_df * repetitions, 8))
        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
        #   loop over elements in new_matrix_dataset
        for i in range(0, nRow_validationSet_no_noise_df):
            #   loop over repetitions
            for j in range(0, repetitions):
                #   index for element in final_matrix_dataset
                idx = i * repetitions + j
                #   create a position with current lat and lon
                pos_tmp = position(validationSet_no_noise_mat[i, 1], validationSet_no_noise_mat[i, 2])
                #   obtain a noisy position
                noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
                noisy_pos = noise[0]

                #   check whether the noisy position is within the limits
                while noisy_pos[0] < noisy_min_lat or \
                        noisy_pos[0] > noisy_max_lat or \
                        noisy_pos[1] < noisy_min_lon or \
                        noisy_pos[1] > noisy_max_lon:
                    noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
                    noisy_pos = noise[0]

                #   update final_matrix_dataset with a new sample
                noisy_validation_set_mat[idx, 0] = validationSet_no_noise_mat[i, 0]
                noisy_validation_set_mat[idx, 1] = noisy_pos[0]
                noisy_validation_set_mat[idx, 2] = noisy_pos[1]
                noisy_validation_set_mat[idx, 3] = validationSet_no_noise_mat[i, 3]
                # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
                #   update matrix for adversarial validation set with noise
                adv_validation_set_mat[idx, 0] = pos_tmp.latitude
                adv_validation_set_mat[idx, 1] = pos_tmp.longitude
                adv_validation_set_mat[idx, 2] = noise[3]
                adv_validation_set_mat[idx, 3] = noise[2]
                adv_validation_set_mat[idx, 4] = noisy_pos[0]
                adv_validation_set_mat[idx, 5] = noisy_pos[1]
                adv_validation_set_mat[idx, 6] = validationSet_no_noise_mat[i, 0]
                adv_validation_set_mat[idx, 7] = validationSet_no_noise_mat[i, 3]
                # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#

        #   check whether there are problems or not
        for i in range(0, noisy_validation_set_mat.shape[0]):
            if noisy_validation_set_mat[i, 1] < noisy_min_lat or noisy_validation_set_mat[i, 1] > noisy_max_lat:
                sys.exit("Some elements are not in the noisy lat range")
            if noisy_validation_set_mat[i, 2] < noisy_min_lon or noisy_validation_set_mat[i, 2] > noisy_max_lon:
                sys.exit("Some elements are not in the noisy lon range")
    # print min(final_matrix_dataset[:, 0])
    # print max(final_matrix_dataset[:, 0])
    # print min(final_matrix_dataset[:, 1])
    # print max(final_matrix_dataset[:, 1])
    # print min(final_matrix_dataset[:, 2])
    # print max(final_matrix_dataset[:, 2])

    # print noisy_training_set_mat
    # print noisy_test_set_mat
    # for i in range(0, 3):
    #    minim = np.min(noisy_training_set_mat[:, i])
    #    maxim = np.max(noisy_training_set_mat[:, i])
    #    print "For the ", str(i), "th column the min is ", str(minim), " and the max is ", str(maxim)

    # for i in range(0, 3):
    #    minim = np.min(noisy_test_set_mat[:, i])
    #    maxim = np.max(noisy_test_set_mat[:, i])
    #    print "For the ", str(i), "th column the min is ", str(minim), " and the max is ", str(maxim)

    if validationSet_no_noise_df is not None:
        return [noisy_training_set_mat, noisy_validation_set_mat, noisy_test_set_mat,
                adv_training_set_mat, adv_validation_set_mat, adv_test_set_mat,
                noisy_min_lat, noisy_min_lon, noisy_max_lat, noisy_max_lon, colNames]
    else:
        return [noisy_training_set_mat, noisy_test_set_mat,
                adv_training_set_mat, adv_test_set_mat,
                noisy_min_lat, noisy_min_lon, noisy_max_lat, noisy_max_lon, colNames]
