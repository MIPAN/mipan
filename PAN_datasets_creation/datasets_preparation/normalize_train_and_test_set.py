import numpy as np
from scaler_between_minus_one_and_one import *
import pandas as pn

def normalize_train_and_test_set(training_set_input_mat, test_set_input_mat,
                                 noisy_min_lat, noisy_max_lat, noisy_min_lon, noisy_max_lon,
                                 colNames, adv_training_set_mat, adv_test_set_mat, validation_set_input_mat=None,
                                 adv_validation_set_mat=None):
    #   create a matrix for the scaled training set matrix
    scaled_training_set_mat = np.zeros((training_set_input_mat.shape))
    scaled_training_set_mat[:, 0] = training_set_input_mat[:, 0]
    scaled_training_set_mat[:, 3] = training_set_input_mat[:, 3]
    if validation_set_input_mat is not None:
        #   create a matrix for the scaled validation set matrix
        scaled_validation_set_mat = np.zeros((validation_set_input_mat.shape))
        scaled_validation_set_mat[:, 0] = validation_set_input_mat[:, 0]
        scaled_validation_set_mat[:, 3] = validation_set_input_mat[:, 3]
    #   create a matrix for the scaled test set matrix
    scaled_test_set_mat = np.zeros((test_set_input_mat.shape))
    scaled_test_set_mat[:, 0] = test_set_input_mat[:, 0]
    scaled_test_set_mat[:, 3] = test_set_input_mat[:, 3]

    #   scaling training set
    scaled_training_set_mat[:, 1] = scaler_between_minus_one_and_one(
        column=training_set_input_mat[:, 1], min_column=noisy_min_lat, max_column=noisy_max_lat)
    scaled_training_set_mat[:, 2] = scaler_between_minus_one_and_one(
        column=training_set_input_mat[:, 2], min_column=noisy_min_lon, max_column=noisy_max_lon)
    #   check whether the values are in the right range
    for i in range(0, 4):
        minim = np.min(scaled_training_set_mat[:, i])
        maxim = np.max(scaled_training_set_mat[:, i])
        print "For the ", str(i), "th column the min is ", str(minim), " and the max is ", str(maxim)
    if validation_set_input_mat is not None:
        #   scaling validation set
        scaled_validation_set_mat[:, 1] = scaler_between_minus_one_and_one(
            column=validation_set_input_mat[:, 1], min_column=noisy_min_lat, max_column=noisy_max_lat)
        scaled_validation_set_mat[:, 2] = scaler_between_minus_one_and_one(
            column=validation_set_input_mat[:, 2], min_column=noisy_min_lon, max_column=noisy_max_lon)
        #   check whether the values are in the right range
        for i in range(0, 4):
            minim = np.min(scaled_validation_set_mat[:, i])
            maxim = np.max(scaled_validation_set_mat[:, i])
            print "For the ", str(i), "th column the min is ", str(minim), " and the max is ", str(maxim)

    #   scaling test set
    scaled_test_set_mat[:, 1] = scaler_between_minus_one_and_one(
        column=test_set_input_mat[:, 1], min_column=noisy_min_lat, max_column=noisy_max_lat)
    scaled_test_set_mat[:, 2] = scaler_between_minus_one_and_one(
        column=test_set_input_mat[:, 2], min_column=noisy_min_lon, max_column=noisy_max_lon)
    #   check whether the values are in the right range
    for i in range(0, 4):
        minim = np.min(scaled_test_set_mat[:, i])
        maxim = np.max(scaled_test_set_mat[:, i])
        print "For the ", str(i), "th column the min is ", str(minim), " and the max is ", str(maxim)

    print "scaled_training_set_mat\n", scaled_training_set_mat
    if validation_set_input_mat is not None:
        print "scaled_validation_set_mat\n", scaled_validation_set_mat
    print "scaled_test_set_mat\n", scaled_test_set_mat

    # save scaled_training_set_mat in a df
    scaled_noisy_training_set_df = pn.DataFrame(data=scaled_training_set_mat,
                                                columns=colNames)
    scaled_noisy_training_set_df['userID'] = scaled_noisy_training_set_df['userID'].astype(np.int64)

    if validation_set_input_mat is not None:
        # save scaled_validation_set_mat in a df
        scaled_noisy_validation_set_df = pn.DataFrame(data=scaled_validation_set_mat,
                                                      columns=colNames)
        scaled_noisy_validation_set_df['userID'] = scaled_noisy_validation_set_df['userID'].astype(np.int64)

    # save scaled_test_set_mat in a df
    scaled_noisy_test_set_df = pn.DataFrame(data=scaled_test_set_mat,
                                            columns=colNames)
    scaled_noisy_test_set_df['userID'] = scaled_noisy_test_set_df['userID'].astype(np.int64)

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    # create a matrix for the scaled_adv_training_set_mat
    scaled_adv_training_set_mat = np.zeros((adv_training_set_mat.shape))
    scaled_adv_training_set_mat[:, 2] = adv_training_set_mat[:, 2]
    scaled_adv_training_set_mat[:, 3] = adv_training_set_mat[:, 3]
    scaled_adv_training_set_mat[:, 6] = adv_training_set_mat[:, 6]
    scaled_adv_training_set_mat[:, 7] = adv_training_set_mat[:, 7]

    #   scaling training set
    scaled_adv_training_set_mat[:, 0] = scaler_between_minus_one_and_one(
        column=adv_training_set_mat[:, 0], min_column=noisy_min_lat, max_column=noisy_max_lat)
    scaled_adv_training_set_mat[:, 1] = scaler_between_minus_one_and_one(
        column=adv_training_set_mat[:, 1], min_column=noisy_min_lon, max_column=noisy_max_lon)
    scaled_adv_training_set_mat[:, 4] = scaler_between_minus_one_and_one(
        column=adv_training_set_mat[:, 4], min_column=noisy_min_lat, max_column=noisy_max_lat)
    scaled_adv_training_set_mat[:, 5] = scaler_between_minus_one_and_one(
        column=adv_training_set_mat[:, 5], min_column=noisy_min_lon, max_column=noisy_max_lon)

    #   save scaled_adv_training_set_mat in a df
    scaled_adv_training_set_df = pn.DataFrame(data=scaled_adv_training_set_mat,
                                              columns=['scaled_orig_lat', 'scale_orig_lon', 'angle_seed',
                                                       'radius_seed', 'scaled_noisy_lat', 'scaled_noisy_lon',
                                                       'userID', 'idPosition'])
    scaled_adv_training_set_df['userID'] = scaled_adv_training_set_df['userID'].astype(np.int64)
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    if adv_validation_set_mat is not None:
        # create a matrix for the scaled_adv_training_set_mat
        scaled_adv_validation_set_mat = np.zeros((adv_validation_set_mat.shape))
        scaled_adv_validation_set_mat[:, 2] = adv_validation_set_mat[:, 2]
        scaled_adv_validation_set_mat[:, 3] = adv_validation_set_mat[:, 3]
        scaled_adv_validation_set_mat[:, 6] = adv_validation_set_mat[:, 6]
        scaled_adv_validation_set_mat[:, 7] = adv_validation_set_mat[:, 7]

        #   scaling training set
        scaled_adv_validation_set_mat[:, 0] = scaler_between_minus_one_and_one(
            column=adv_validation_set_mat[:, 0], min_column=noisy_min_lat, max_column=noisy_max_lat)
        scaled_adv_validation_set_mat[:, 1] = scaler_between_minus_one_and_one(
            column=adv_validation_set_mat[:, 1], min_column=noisy_min_lon, max_column=noisy_max_lon)
        scaled_adv_validation_set_mat[:, 4] = scaler_between_minus_one_and_one(
            column=adv_validation_set_mat[:, 4], min_column=noisy_min_lat, max_column=noisy_max_lat)
        scaled_adv_validation_set_mat[:, 5] = scaler_between_minus_one_and_one(
            column=adv_validation_set_mat[:, 5], min_column=noisy_min_lon, max_column=noisy_max_lon)

        #   save scaled_adv_validation_set_mat in a df
        scaled_adv_validation_set_df = pn.DataFrame(data=scaled_adv_validation_set_mat,
                                                    columns=['scaled_orig_lat', 'scale_orig_lon', 'angle_seed',
                                                             'radius_seed', 'scaled_noisy_lat', 'scaled_noisy_lon',
                                                             'userID', 'idPosition'])
        scaled_adv_validation_set_df['userID'] = scaled_adv_validation_set_df['userID'].astype(np.int64)
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    # create a matrix for the scaled_adv_training_set_mat
    scaled_adv_test_set_mat = np.zeros((adv_test_set_mat.shape))
    scaled_adv_test_set_mat[:, 2] = adv_test_set_mat[:, 2]
    scaled_adv_test_set_mat[:, 3] = adv_test_set_mat[:, 3]
    scaled_adv_test_set_mat[:, 6] = adv_test_set_mat[:, 6]
    scaled_adv_test_set_mat[:, 7] = adv_test_set_mat[:, 7]

    #   scaling training set
    scaled_adv_test_set_mat[:, 0] = scaler_between_minus_one_and_one(
        column=adv_test_set_mat[:, 0], min_column=noisy_min_lat, max_column=noisy_max_lat)
    scaled_adv_test_set_mat[:, 1] = scaler_between_minus_one_and_one(
        column=adv_test_set_mat[:, 1], min_column=noisy_min_lon, max_column=noisy_max_lon)
    scaled_adv_test_set_mat[:, 4] = scaler_between_minus_one_and_one(
        column=adv_test_set_mat[:, 4], min_column=noisy_min_lat, max_column=noisy_max_lat)
    scaled_adv_test_set_mat[:, 5] = scaler_between_minus_one_and_one(
        column=adv_test_set_mat[:, 5], min_column=noisy_min_lon, max_column=noisy_max_lon)

    #   save scaled_adv_test_set_mat in a df
    scaled_adv_test_set_df = pn.DataFrame(data=scaled_adv_test_set_mat,
                                          columns=['scaled_orig_lat', 'scale_orig_lon', 'angle_seed',
                                                   'radius_seed', 'scaled_noisy_lat', 'scaled_noisy_lon',
                                                   'userID', 'idPosition'])
    scaled_adv_test_set_df['userID'] = scaled_adv_test_set_df['userID'].astype(np.int64)
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    if adv_validation_set_mat is not None and validation_set_input_mat is not None:
        return [scaled_noisy_training_set_df, scaled_noisy_validation_set_df, scaled_noisy_test_set_df,
                scaled_adv_training_set_df, scaled_adv_validation_set_df, scaled_adv_test_set_df]
    else:
        return [scaled_noisy_training_set_df, scaled_noisy_test_set_df,
                scaled_adv_training_set_df, scaled_adv_test_set_df]

