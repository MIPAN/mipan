"""
read a .pickle file and load it into a dataframe
"""

import pickle
from bcolors import *


def loadDataset(inputFile):
    with open(inputFile) as f:
        print bcolors.OKGREEN + "Loading file..." + bcolors.ENDC
        loaded_obj = pickle.load(f)
        #print bcolors.OKDARKTHEME + str(loaded_obj.dtypes) + bcolors.ENDC
        #print bcolors.OKDARKTHEME + "(rows, cols) = " + str(loaded_obj.shape) + bcolors.ENDC
        return loaded_obj
