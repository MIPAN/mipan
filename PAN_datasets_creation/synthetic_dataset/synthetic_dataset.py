from createSquareLimit import create_square_limit
import numpy as np
import pickle
from position import *
from addLatLongNoise import *
from random import *
from haversine import *
import pandas as pn

def synthetic_dataset(central_position, side_length, epsilon):

    # given a central position and a side length define a square by its vertices
    min_lat, max_lat, min_longit, max_longit = \
        create_square_limit(central_position=central_position, side_length=side_length)

    print "min_lat ",  min_lat
    print "max_lat ", max_lat
    print "min_longit ", min_longit
    print "max_longit ", max_longit

    v0 = position(max_lat, max_longit)
    v1 = position(min_lat, max_longit)
    v2 = position(min_lat, min_longit)
    v3 = position(max_lat, min_longit)

    #   create the list of vertices
    vertices = [v0, v1, v2, v3]
    #   create the matrix of vertices
    vertices_mat = np.empty((4, 3))
    #   populate the matrix of vertices: correspondence one-to-one between a vertex and a class
    vertices_mat[0, 0] = 0
    vertices_mat[0, 1] = v0.latitude
    vertices_mat[0, 2] = v0.longitude

    vertices_mat[1, 0] = 1
    vertices_mat[1, 1] = v1.latitude
    vertices_mat[1, 2] = v1.longitude

    vertices_mat[2, 0] = 2
    vertices_mat[2, 1] = v2.latitude
    vertices_mat[2, 2] = v2.longitude

    vertices_mat[3, 0] = 3
    vertices_mat[3, 1] = v3.latitude
    vertices_mat[3, 2] = v3.longitude

    #   600 points for each class 600 * 4 = 2400
    total_points = 2400

    #   matrix with all the points
    all_positions = np.empty((total_points, 4))

    #   number of points for each class
    points_in_all_sets_for_each_class = total_points // len(vertices)

    #   for each vertex, meaning for each class
    for i in range(0, len(vertices)):
        #   loop over repetitions
        for j in range(0, points_in_all_sets_for_each_class):
            #   index for element in noisy_training_set_mat
            idx = i * points_in_all_sets_for_each_class + j
            #   create a position with current lat and lon
            pos_tmp = vertices[i]
            #   obtain a noisy position
            noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
            noisy_pos = noise[0]

            while(len(np.where((all_positions[:, 1] == noisy_pos[0]) & (all_positions[:, 2] == noisy_pos[1]))[0]) > 0):
                noise = addPolarNoise(epsilon=epsilon, pos=pos_tmp, str_ctrl="appr")
                noisy_pos = noise[0]

            all_positions[idx, 0] = i
            all_positions[idx, 1] = noisy_pos[0]
            all_positions[idx, 2] = noisy_pos[1]
            all_positions[idx, 3] = idx

    for i in range(0, len(vertices)):
        idx_of_elements_in_class_i = np.where(all_positions[:, 0] == i)[0]
        #print idx_of_elements_in_class_i
        max_squared_distances = 0
        v_lat = vertices_mat[i, 1]
        v_lon = vertices_mat[i, 2]
        vertex = (v_lat, v_lon)

        for j in range(0, len(idx_of_elements_in_class_i)):
            current_point = (all_positions[idx_of_elements_in_class_i[j], 1],
                             all_positions[idx_of_elements_in_class_i[j], 2])
            #print vertex
            #print current_point
            if (haversine(vertex, current_point) * 1000) > max_squared_distances:
                max_squared_distances = haversine(vertex, current_point) * 1000
            #sum_squared_distances += haversine(vertex, current_point) * 1000

        #print "avg distance from vertex for points of class ", i, " ", \
        #    sum_squared_distances / float(len(idx_of_elements_in_class_i))
        print 'max squared distance from vertex for points of class ', i, " ", max_squared_distances

    """
    with open("/home/comete/mromanel/synthetic_dataset_experiments/original_synthetic_datasets_no_noise_no_scaling",
              'wb') as f:
        pickle.dump(pn.DataFrame(data=all_positions, columns=["userID", "latitude", "longitude"]), f)
    with open("/home/comete/mromanel/synthetic_dataset_experiments/vertices",
              'wb') as f:
        pickle.dump(vertices_mat, f)
    """
    return [all_positions, vertices_mat]