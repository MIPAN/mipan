from datasets_preparation import loadDataset
from keras.models import load_model
from keras import Input, Model
from keras.layers import Dense
import os
import tensorflow as tf
import numpy as np
import operator
from keras import utils
from keras import optimizers
from keras import callbacks as cb
from keras import backend as K
import pandas as pn
import sys
import random
import pickle

####################################################  MI computation  ##################################################
def condition(i, number_of_elements, y_true, y_pred, M):
    return i < number_of_elements


def condition2(i, number_of_elements, Mx, Mx_tmp):
    return i < number_of_elements


def body(i, number_of_elements, y_true, y_pred, M):
    tmp = tf.matmul(tf.gather(y_pred, i, axis=0), tf.transpose(tf.gather(y_true, i, axis=0)))
    tmp = tf.reshape(tmp, shape=[1, tf.shape(y_true)[1], tf.shape(y_true)[1]])
    M = tf.concat([M, tmp], axis=0)
    return i + 1, number_of_elements, y_true, y_pred, M


def body2(i, number_of_elements, Mx, Mx_tmp):
    Mx = tf.concat([Mx, Mx_tmp], axis=0)
    return i + 1, number_of_elements, Mx, Mx_tmp


def condition_H_true(iter, H_true, P_marg_true):
    return iter < tf.shape(P_marg_true)[0]


def body_H_true(iter, H_true, P_marg_true):
    fix = tf.constant(0.0001)
    p_x = tf.gather(P_marg_true, iter, axis=0)
    a = tf.to_float(fix - (fix * p_x) + p_x)
    b = tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))
    H_true -= tf.to_float(a) * (tf.log(tf.to_float(a)) / b)
    return iter + 1, H_true, P_marg_true


def condition_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    return jter < tf.shape(P_marg_true)[0]


def condition_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    return kter < tf.shape(P_marg_pred)[0]


def body_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    kter = tf.constant(0)
    tmp = tf.constant([0.0])

    jter, kter, tmp, P_marg_pred, P_jk = tf.while_loop(condition_H_true_cond_pred_2,
                                                       body_H_true_cond_pred_2,
                                                       [jter, kter, tmp, P_marg_pred, P_jk])

    H_true_cond_pred += tmp
    return jter+1, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk


def body_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    fix = tf.constant(0.0001)
    a = fix - (fix * P_jk[kter, jter]) + P_jk[kter, jter]
    b = fix - (fix * P_marg_pred[kter]) + P_marg_pred[kter]
    tmp += -(a * (tf.log(a / b) / tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))))
    return jter, kter+1, tmp, P_marg_pred, P_jk


def MI_loss_G(y_true, y_pred):
    #return K.categorical_crossentropy(y_true, y_pred)

    rank = len(y_pred.shape)
    axis = -1 % rank
    y_pred = y_pred / tf.reduce_sum(y_pred, axis, True)

    y_true = tf.reshape(y_true, [tf.shape(y_true)[0], tf.shape(y_true)[1], 1])
    y_pred = tf.reshape(y_pred, [tf.shape(y_pred)[0], tf.shape(y_pred)[1], 1])

    # y_true = tf.Print(y_true, [y_true], message="y_true ---> ", summarize=10)
    number_of_elements = tf.shape(y_true)[0]
    number_of_elements = tf.Print(number_of_elements,
                                  [number_of_elements],
                                  message="number_of_elements ---> ", summarize=10)

    i = tf.constant(1)
    M = tf.matmul(tf.gather(y_pred, 0, axis=0), tf.transpose(tf.gather(y_true, 0, axis=0)))
    M = tf.reshape(M, shape=[1, 4, 4])

    i, number_of_elements, y_true, y_pred, M = tf.while_loop(
        cond=condition,
        body=body,
        loop_vars=[i, number_of_elements, y_true, y_pred, M],
        shape_invariants=[i.get_shape(),
                          number_of_elements.get_shape(),
                          y_true.get_shape(),
                          y_pred.get_shape(),
                          tf.TensorShape([None, M.get_shape().as_list()[1], M.get_shape().as_list()[2]])])
    # tf.TensorShape(None)])
    # tf.TensorShape([None, 4, 4])])

    F_jk = tf.reduce_sum(M, axis=0)
    P_jk = tf.to_float(F_jk) / tf.to_float(number_of_elements)

    P_marg_true = tf.to_float(tf.reduce_sum(y_true, axis=0)) / tf.to_float(number_of_elements)

    P_marg_pred = tf.to_float(tf.reduce_sum(y_pred, axis=0)) / tf.to_float(number_of_elements)

    iter = tf.constant(0)
    H_true = tf.constant([0.0])

    iter, H_true, P_marg_true = tf.while_loop(condition_H_true, body_H_true, [iter, H_true, P_marg_true])

    H_true = tf.Print(H_true, [H_true], message="H_true")

    H_true_cond_pred = tf.constant([0.0])
    jter = tf.constant(0)


    jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk = tf.while_loop(condition_H_true_cond_pred,
                                                                           body_H_true_cond_pred,
                                                                           [jter,
                                                                            H_true_cond_pred,
                                                                            P_marg_pred,
                                                                            P_marg_true,
                                                                            P_jk])

    H_true_cond_pred = tf.Print(H_true_cond_pred, [H_true_cond_pred], message="H_true_cond_pred")

    MI = H_true - H_true_cond_pred

    return MI

def MI_loss_C(y_true, y_pred):
    #return K.categorical_crossentropy(y_true, y_pred)

    rank = len(y_pred.shape)
    axis = -1 % rank
    y_pred = y_pred / tf.reduce_sum(y_pred, axis, True)

    y_true = tf.reshape(y_true, [tf.shape(y_true)[0], tf.shape(y_true)[1], 1])
    y_pred = tf.reshape(y_pred, [tf.shape(y_pred)[0], tf.shape(y_pred)[1], 1])

    # y_true = tf.Print(y_true, [y_true], message="y_true ---> ", summarize=10)
    number_of_elements = tf.shape(y_true)[0]
    number_of_elements = tf.Print(number_of_elements,
                                  [number_of_elements],
                                  message="number_of_elements ---> ", summarize=10)

    i = tf.constant(1)
    M = tf.matmul(tf.gather(y_pred, 0, axis=0), tf.transpose(tf.gather(y_true, 0, axis=0)))
    M = tf.reshape(M, shape=[1, 4, 4])

    i, number_of_elements, y_true, y_pred, M = tf.while_loop(
        cond=condition,
        body=body,
        loop_vars=[i, number_of_elements, y_true, y_pred, M],
        shape_invariants=[i.get_shape(),
                          number_of_elements.get_shape(),
                          y_true.get_shape(),
                          y_pred.get_shape(),
                          tf.TensorShape([None, M.get_shape().as_list()[1], M.get_shape().as_list()[2]])])
    # tf.TensorShape(None)])
    # tf.TensorShape([None, 4, 4])])

    F_jk = tf.reduce_sum(M, axis=0)
    P_jk = tf.to_float(F_jk) / tf.to_float(number_of_elements)

    P_marg_true = tf.to_float(tf.reduce_sum(y_true, axis=0)) / tf.to_float(number_of_elements)

    P_marg_pred = tf.to_float(tf.reduce_sum(y_pred, axis=0)) / tf.to_float(number_of_elements)

    iter = tf.constant(0)
    H_true = tf.constant([0.0])

    iter, H_true, P_marg_true = tf.while_loop(condition_H_true, body_H_true, [iter, H_true, P_marg_true])

    H_true = tf.Print(H_true, [H_true], message="H_true")

    H_true_cond_pred = tf.constant([0.0])
    jter = tf.constant(0)


    jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk = tf.while_loop(condition_H_true_cond_pred,
                                                                           body_H_true_cond_pred,
                                                                           [jter,
                                                                            H_true_cond_pred,
                                                                            P_marg_pred,
                                                                            P_marg_true,
                                                                            P_jk])

    H_true_cond_pred = tf.Print(H_true_cond_pred, [H_true_cond_pred], message="H_true_cond_pred")

    MI = H_true - H_true_cond_pred

    return -MI

########################################################################################################################

def build_classifier():
    hiddenLayersCard = 3
    hiddenNeuronsCard = [60, 100, 51, 66, 49, 65, 61, 49, 63, 100]
    # inputs of 2D vectors
    input_layer = Input(shape=(2,), name='discriminator_input')
    # at least one hidden layer
    hidden_layer = Dense(hiddenNeuronsCard[0], activation='relu', name='hidden_layer_0')(input_layer)
    # other hidden layers if any
    for idx in range(1, hiddenLayersCard):
        layer_name = 'hidden_layer_' + str(idx)
        hidden_layer = Dense(hiddenNeuronsCard[idx], activation='relu', name=layer_name)(hidden_layer)
    # output layer
    output_layer = Dense(4, activation='softmax', name='output_layer')(hidden_layer)

    #   create the network model
    discriminator = Model(inputs=input_layer, outputs=output_layer, name='discriminatorANN')

    # parallel_model = multi_gpu_model(discriminator, gpus=2)
    # parallel_model.compile(loss='categorical_crossentropy',
    #                       optimizer='adam',
    #                       metrics=['categorical_accuracy'])
    """
    discriminator.compile(loss=MI_loss,
                          optimizer='adam',
                          metrics=['categorical_accuracy'])
    """

    discriminator.compile(loss=MI_loss_C, optimizer='adam')

    # print(parallel_model.summary())
    print discriminator.summary()

    return discriminator

#   new loss function, sigmoid centered in 300 so that we can compare the distance we obtain, i.e. MSE, to this one
def adv_euclid_700_meters(y_true, y_pred):
    #   compute the mean squared error for the obtained value and supervision
    y_true = K.cast(y_true, dtype='float32')
    y_pred = K.cast(y_pred, dtype='float32')
    mse = tf.sqrt(tf.reduce_sum(tf.square(y_true - y_pred), reduction_indices=1))
    me = K.mean(mse)
    me = tf.scalar_mul(scalar=3250, x=me)
    me = me - 250
    # me = tf.scalar_mul(scalar=0.1, x=me)
    me = tf.nn.softplus(me)
    #me = tf.square(me)
    return me


#   setu for GPU usage
def GPU_setup(id_GPU, memory_percentage):
    #   only use GPU with index id_GPU
    #   id_GPU is a string like "0"
    os.environ["CUDA_VISIBLE_DEVICES"] = id_GPU

    #   TensorFlow wizardry
    config = tf.ConfigProto()

    #   Don't pre_allocate memory; allocate as_needed
    config.gpu_options.allow_growth = True

    #   Only allow a total of half the GPU memory to be allocated
    #   memory_percentage is a float between 0 and 1
    config.gpu_options.per_process_gpu_memory_fraction = memory_percentage

    #   Create a session with the above options specified.
    K.tensorflow_backend.set_session(tf.Session(config=config))


#   one hot coding for discriminator output
#   weight computation for dataset classification balancing
def one_hot_coding_and_weight_computation(supervision):
    #   one-hot coding user_id

    #   get unique user_id
    supervisionUnique, counts = np.unique(supervision, return_counts=True)
    #   transform the result of the previous operation in a python dict so we can sort it
    #   zip associates id with number of occurences
    #   then a dict gets built
    dict_ = dict(zip(supervisionUnique, counts))
    # print dict_
    #   sorting the value of unique in order of increasing id
    sorted_x = sorted(dict_.items(), key=operator.itemgetter(0), )
    # print "sorted_x ---> ", sorted_x

    #   max occurrence
    max_occurrence = -float('inf')
    for i in range(0, len(sorted_x)):
        if sorted_x[i][1] > max_occurrence:
            max_occurrence = sorted_x[i][1]
    #   print max_occurrence

    weight_classes_dictionary = {}
    for j in range(0, len(sorted_x)):
        #   the weight for a class is the number of occurrences of the most frequent class over the occurrences of the
        #   class whose weight I am computing
        weight_classes_dictionary[j] = max_occurrence / float(sorted_x[j][1])
    # print "weight_classes_dictionary", weight_classes_dictionary
    #   change all user_id to their unique coding version (from 0 to len(unique(supervision))-1)
    oldClass_newClass_dict = {}
    for i in range(0, len(supervision)):
        for j in range(0, len(sorted_x)):
            if supervision[i] == sorted_x[j][0]:
                oldClass_newClass_dict[j] = supervision[i]
                supervision[i] = j
    # print "oldClass_newClass_dict ---> ", oldClass_newClass_dict
    #   one-hot coding of supervision
    supervision = utils.to_categorical(supervision)
    cont = 0
    for i in range(0, len(supervision)):
        if supervision[i][2] == 1:
            cont += 1
    # print cont
    # print len(supervision)
    return [supervision, weight_classes_dictionary]


def build_adv_model(generator_model_file, trained_weights_file=None):

    #   load the discriminator model
    #discriminatorLayer = load_model(filepath=discrimininator_model_file)
    discriminatorLayer = build_classifier()
    #   load the generator model
    generatorLayer = load_model(filepath=generator_model_file)

    ####################################################################
    ##############################  dNet  ##############################
    ####################################################################

    #   create the architecture for the discriminator where the the discriminator layer is active
    input_layer_discriminator = Input(shape=(2,), name='discriminator_Input')
    discriminatorLayer.trainable = True
    output_layer_discriminator = discriminatorLayer(input_layer_discriminator)

    #   create the discriminator model
    discriminator = Model(inputs=input_layer_discriminator, outputs=output_layer_discriminator,
                          name="discriminator")

    #   compile discriminator model
    """
    discriminator.compile(loss='categorical_crossentropy',
                          optimizer='adam',
                          metrics=['categorical_accuracy'])
    """
    discriminator.compile(loss=MI_loss_C,
                          optimizer='adam')

    trainable_weigths_discriminator_network = len(discriminator.trainable_weights)
    print "trainable_weigths_discriminator_network ---> ", trainable_weigths_discriminator_network
    ####################################################################
    ##############################  advNet  ############################
    ####################################################################

    #   create an input layer for the advNet, it must have the same dimension as the gNet input
    input_layer_advNet = Input(shape=(4,), name='adversarial_Input')

    #   create the architecture for the adversarial net where the the discriminator model is not active and the
    #   generative layer is active
    discriminator.trainable = False
    generatorLayer.trainable = True

    trainable_weigths_generator_network = len(generatorLayer.trainable_weights)
    print "trainable_weigths_generator_network ---> ", trainable_weigths_generator_network

    #   create advNet model architecture
    #   this model has one inout and two outputs
    #   two outputs are needed to check two loss functions
    #   the first output is the output of the generator layer
    #   the second outpu is the output of the discriminator module when it receives the generatorLayer output
    advNet = Model(input_layer_advNet, [generatorLayer(input_layer_advNet),
                                        discriminator(generatorLayer(input_layer_advNet))])

    #   we have two losses one is used to minize the mse between the input and the output of the generatorLayer while
    #   the other one is used to minize the opposite of the categorical_crossentropy so to maximize the distance between
    #   target and prediction for the classifier
    optimizer_adv = optimizers.Adam(lr=0.0001)
    alpha = 1.0#K.variable(float(sys.argv[5]))
    beta = 1.0#K.variable(float(sys.argv[6]))
    loss_weights = [alpha, beta]
    """
    advNet.compile(loss=[adv_euclid_700_meters, opposite_categorical_crossentropy],
                   loss_weights=loss_weights,
                   optimizer='adam',
                   metrics=['categorical_accuracy'])
    """
    advNet.compile(loss=[adv_euclid_700_meters, MI_loss_G],
                   loss_weights=loss_weights,
                   optimizer='adam')

    trainable_weigths_adversarial_network = len(advNet.trainable_weights)
    print "trainable_weigths_adversarial_network ---> ", trainable_weigths_adversarial_network

    print discriminator.summary()
    print advNet.summary()

    print "trainable_weigths_discriminator_network ---> ", trainable_weigths_discriminator_network
    print "trainable_weigths_generator_network ---> ", trainable_weigths_generator_network
    print "trainable_weigths_adversarial_network ---> ", trainable_weigths_adversarial_network

    if trained_weights_file is not None:
        advNet.load_weights(filepath=trained_weights_file)
        print "CIAOO"
    return advNet


def prediction_for_plots_syntethic(inputFile, testfile, generator_model_file, prediction_file_path, training_index_file,
                                   validation_index_file=None, trained_weights_file=None):
    GPU_setup(id_GPU="3", memory_percentage=0.2)
    #   pass the training adv version of the noisy dataset
    dataset = loadDataset.loadDataset(inputFile=inputFile)
    if training_index_file is not None:
        training_index = loadDataset.loadDataset(inputFile=training_index_file)
        training_data = dataset.values[training_index, :]
    if validation_index_file is not None:
        #   load the indexes of the data in the training adv version of the noisy dataset used for validation
        validation_index = loadDataset.loadDataset(inputFile=validation_index_file)

        considered_data = dataset.values[validation_index, :]
        print "considered_data.shape--->", considered_data.shape


    else:
        considered_data = dataset.values

    test_mat = loadDataset.loadDataset(testfile).values

    """
    ################################################################ ORIGINAL POSITIONS
    ids_positions = np.unique(considered_data[:, 7])
    res_mat_orig_pos = np.zeros(shape=(len(ids_positions), 3))

    for i in range(0, len(ids_positions)):
        idx_orig_pos = np.where(considered_data[:, 7] == ids_positions[i])[0][0]
        res_mat_orig_pos[i, 0] = considered_data[idx_orig_pos, 0]
        res_mat_orig_pos[i, 1] = considered_data[idx_orig_pos, 1]
        if considered_data[idx_orig_pos, 6] == 39199:
            res_mat_orig_pos[i, 2] = 2

        elif considered_data[idx_orig_pos, 6] == 3003:
            res_mat_orig_pos[i, 2] = 4

        elif considered_data[idx_orig_pos, 6] == 1671:
            res_mat_orig_pos[i, 2] = 6

        else:
            res_mat_orig_pos[i, 2] = 0
        
        with open(original_dataset_positions_file, 'wb') as f:
            pickle.dump(res_mat_orig_pos, f)

    ################################################################ LAPLACE NOISE
    advNet_laplace = build_adv_model(generator_model_file=generator_model_file,
                                     trained_weights_file=None)

    prediction_laplace = advNet_laplace.predict(x=considered_data[:, 0:4])[0]

    print min(prediction_laplace[:, 0])
    print max(prediction_laplace[:, 0])
    print min(prediction_laplace[:, 1])
    print max(prediction_laplace[:, 1])

    res_mat_laplace = np.zeros(shape=(prediction_laplace.shape[0], prediction_laplace.shape[1] + 1))

    for i in range(0, prediction_laplace.shape[0]):
        res_mat_laplace[i, 0] = prediction_laplace[i, 0]
        res_mat_laplace[i, 1] = prediction_laplace[i, 1]

        if considered_data[i, 6] == 39199:
            res_mat_laplace[i, 2] = 2

        elif considered_data[i, 6] == 3003:
            res_mat_laplace[i, 2] = 4

        elif considered_data[i, 6] == 1671:
            res_mat_laplace[i, 2] = 6

        else:
            res_mat_laplace[i, 2] = 0

    with open(prediction_file_path_laplace, 'wb') as f:
        pickle.dump(res_mat_laplace, f)
    """
    ################################################################ G NETWORK NOISE
    advNet = build_adv_model(generator_model_file=generator_model_file,
                             trained_weights_file=trained_weights_file)

    prediction = advNet.predict(x=considered_data[:, 0:4])[0]

    print min(prediction[:, 0])
    print max(prediction[:, 0])
    print min(prediction[:, 1])
    print max(prediction[:, 1])

    res_mat = np.zeros(shape=(prediction.shape[0], prediction.shape[1] + 2))
    res_mat_2 = np.zeros(shape=(prediction.shape[0], 8))


    for i in range(0, prediction.shape[0]):
        res_mat[i, 0] = prediction[i, 0]
        res_mat[i, 1] = prediction[i, 1]
        res_mat[i, 2] = considered_data[i, 6]
        res_mat[i, 3] = considered_data[i, 7]

        res_mat_2[i, 0] = considered_data[i, 0]
        res_mat_2[i, 1] = considered_data[i, 1]
        res_mat_2[i, 4] = prediction[i, 0]
        res_mat_2[i, 5] = prediction[i, 1]
        res_mat_2[i, 6] = considered_data[i, 6]


    with open(prediction_file_path + '/prediction_G_noise', 'wb') as f:
        pickle.dump(res_mat, f)

    with open(prediction_file_path + '/prediction_G_noise_df', 'wb') as f:
        pickle.dump(pn.DataFrame(data=res_mat_2), f)

    prediction_test = advNet.predict(x=test_mat[:, 0:4])[0]

    res_mat_3 = np.zeros(shape=(prediction_test.shape[0], 8))
    res_mat_4 = np.zeros(shape=(prediction_test.shape[0], prediction_test.shape[1] + 2))

    for i in range(0, prediction_test.shape[0]):
        res_mat_4[i, 0] = prediction_test[i, 0]
        res_mat_4[i, 1] = prediction_test[i, 1]
        res_mat_4[i, 2] = test_mat[i, 6]
        res_mat_4[i, 3] = test_mat[i, 7]

        res_mat_3[i, 0] = test_mat[i, 0]
        res_mat_3[i, 1] = test_mat[i, 1]
        res_mat_3[i, 4] = prediction_test[i, 0]
        res_mat_3[i, 5] = prediction_test[i, 1]
        res_mat_3[i, 6] = test_mat[i, 6]

    with open(prediction_file_path + '/prediction_test_G_noise_df', 'wb') as f:
        pickle.dump(pn.DataFrame(data=res_mat_3), f)
    with open(prediction_file_path + '/prediction_test_G_noise', 'wb') as f:
        pickle.dump(res_mat_4, f)


    """
    ################################################################ EVALUATIONS

    training_data_mat = training_data
    training_supervision = one_hot_coding_and_weight_computation(training_data_mat[:, 6])[0]
    considered_data_mat = considered_data
    sup_cons_dat = one_hot_coding_and_weight_computation(considered_data_mat[:, 6])[0]
    test = loadDataset.loadDataset(inputFile=testFile).values
    supervisionDiscriminator_mat_test = \
        one_hot_coding_and_weight_computation(test[:, 6])[0]

    #   load the discriminator model
    discriminatorLayer = load_model(filepath=discrimininator_model_file)
    print(advNet.metrics_names)

    print training_data_mat.shape
    print advNet.evaluate(x=training_data_mat[:, 0:4],
                          y=[training_data_mat[:, 0:2], training_supervision],
                          batch_size=training_data_mat.shape[0])

    training_predictions = advNet.predict(x=training_data_mat[:, 0:4])[0]
    tr_distance = 0
    for i in range(0, training_predictions.shape[0]):
        tr_distance += (((training_data_mat[i, 0] - training_predictions[i, 0])**2) + ((training_data_mat[i, 1] -
                                                                                        training_predictions[i, 1])
                                                                                       ** 2)) ** 0.5
    tr_distance = tr_distance/float(training_predictions.shape[0])
    tr_distance = tr_distance * float(3250)
    print "tr_distance ---> ", tr_distance

    
    print considered_data_mat.shape
    print advNet.evaluate(x=considered_data_mat[:, 0:4],
                          y=[considered_data_mat[:, 0:2], sup_cons_dat],
                          batch_size=considered_data_mat.shape[0])

    val_predictions = advNet.predict(x=considered_data_mat[:, 0:4])[0]
    val_distance = 0
    for i in range(0, val_predictions.shape[0]):
        val_distance += (((considered_data_mat[i, 0] - val_predictions[i, 0]) ** 2) + ((considered_data_mat[i, 1] -
                                                                                        val_predictions[i, 1])
                                                                                       ** 2)) ** 0.5
    val_distance = val_distance / float(val_predictions.shape[0])
    val_distance = val_distance * float(3250)
    print "val_distance ---> ", val_distance

    print test.shape
    print advNet.evaluate(x=test[:, 0:4],
                          y=[test[:, 0:2], supervisionDiscriminator_mat_test],
                          batch_size=test.shape[0])

    test_predictions = advNet.predict(x=test[:, 0:4])[0]
    test_distance = 0
    for i in range(0, test_predictions.shape[0]):
        test_distance += (((test[i, 0] - test_predictions[i, 0]) ** 2) + ((test[i, 1] -
                                                                           test_predictions[i, 1])
                                                                          ** 2)) ** 0.5
    test_distance = test_distance / float(test_predictions.shape[0])
    test_distance = test_distance * float(3250)
    print "test_distance ---> ", test_distance


    input_same_orig_pos_different_seed = np.zeros((10000, 4))

    orig_scaled_lat = -0.621380930
    orig_scaled_lon = 0.194747489

    for i in range(0, input_same_orig_pos_different_seed.shape[0]):
        input_same_orig_pos_different_seed[i, 0] = orig_scaled_lat
        input_same_orig_pos_different_seed[i, 1] = orig_scaled_lon
        input_same_orig_pos_different_seed[i, 2] = random.random()
        input_same_orig_pos_different_seed[i, 3] = random.random()

    pred = advNet.predict(x=input_same_orig_pos_different_seed)[0]
    
    with open(prediction_fixed_location, 'wb') as f:
        pickle.dump(pred, f)
    """