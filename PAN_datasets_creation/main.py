from datasets_preparation import loadDataset, createDataset_from_txt, get_users_of_interest, split_dataset_for_ML, \
    laplacian_noise_injection, normalize_train_and_test_set
from synthetic_dataset import synthetic_dataset
from files_for_plots import predictions_for_plots, predictions_for_plots_synthetic
import math
from position import *
from createSquareLimit import *
import numpy as np
import pandas as pn
import pickle
import datetime
import sys


def main_func():
    #   sys.argv[1] is the directory for synthetic data
    #   sys.argv[2] is the directory for real data
    #   sys.argv[3] absolute path to original .txt dataset file for Gowalla
    #   sys.argv[4] is the epsilon
    #   sys.argv[5] is the central latitude
    #   sys.argv[6] is the central longitude
    #   sys.argv[7] is the side length
    #   sys.argv[8] is the noisy side length
    #   sys.argv[9] is the cardinality of the users of interest
    #   sys.argv[10] is the synthetic side length
    #   sys.argv[11] is the synthetic epsilon for creating a noise of scattered original points around the vertices

    ########################################################################################################################
    #############################################  SET GLOBAL VARIABLE  ####################################################
    ########################################################################################################################

    original_dataset_txt = sys.argv[3]

    original_dataset_df = sys.argv[2] + "/PAN_datasets/original_df.pickle"

    users_of_interest_most_frequent_check_in_no_scaling_original_positions_df = \
        sys.argv[2] + "/PAN_datasets/users_of_interest_most_frequent_check_in_no_scaling_original_positions_df"

    #   name of the columns for the downloaded database
    original_colnames = ['userID', 'timeStamp', 'latitude', 'longitude', 'locationID']

    #   columns to be dropped from the original dataset
    colsToDrop = ['timeStamp', 'locationID']

    #   name of the columns we are interested in
    cols_of_interest = ['userID', 'latitude', 'longitude']

    #   value for the epsilon variable that we are going to use in our experiments
    epsilon = float(sys.argv[4]) # math.log(2) / 100

    #   central position for the squared area of interest
    #   5, Boulevard Sebastopol, Paris, France
    # central_position = position(lat=48.85831169999999, lon=2.3478809000000638)
    central_position = position(lat=float(sys.argv[5]), lon=float(sys.argv[6]))

    #   length of the side for the squared area of interest: in meters
    side_length = float(sys.argv[7])

    #   length of the side for the squared area of interest for the noisy data: in meters
    noisy_side_length = float(sys.argv[8])

    #   cardinality of the users of interest
    usersOI_card = int(sys.argv[9])

    ########################################################################################################################
    ###########################################  CREATE SYNTHETIC DATASET   ################################################
    ########################################################################################################################

    #   set the side length for the square where the synthetic points are going to be placed, this is done to be consistent
    #   with the same system adopted with the original dataset
    synthetic_side_length = sys.argv[10]

    #   all the syntetic points: the epsilon value is linked to privacy; a small epsilon means a big displacement radius for
    #   the laplacian mechanism and it means that our points are going to be obfuscated with a good privacy but maybe not a
    #   big utility. Here we only want points close to the vertices (30 to 50 meters distance from the correspondent vertex
    #   with good probability so we can pick a relative "big" epsilon
    all_positions_mat, vertices_mat = synthetic_dataset.synthetic_dataset(central_position=central_position,
                                                                          side_length=synthetic_side_length,
                                                                          epsilon=sys.argv[11])
                                                                          #epsilon=math.log(2) / 3)

    all_positions_df = pn.DataFrame(data=all_positions_mat, columns=["userID", "latitude", "longitude", "position_id"])

    #   save the datasets
    with open(sys.argv[1] + '/synthetic_dataset_experiments/original_synthetic_datasets_no_noise_no_scaling',
              'wb') as f:
        pickle.dump(all_positions_df, f)
    with open(sys.argv[1] + '/synthetic_dataset_experiments/vertices',
              'wb') as f:
        pickle.dump(vertices_mat, f)

    #   split the dataset into training and test set
    datasetToSplit = all_positions_df
    trainingSet_no_noise_df, testSet_no_noise_df = split_dataset_for_ML.datasetSplit(datasetToSplit=datasetToSplit,
                                                                                     testPercentage=20)
    # print trainingSet_no_noise_df
    # print testSet_no_noise_df

    #   create datasets for classification without noise injection
    noisy_training_set_mat, noisy_test_set_mat, adv_training_set_mat, adv_test_set_mat, noisy_min_lat, \
    noisy_min_lon, noisy_max_lat, noisy_max_lon, colNames = laplacian_noise_injection.noiseInjection(
        trainingSet_no_noise_df=trainingSet_no_noise_df,
        testSet_no_noise_df=testSet_no_noise_df,
        repetitions=1,
        epsilon=epsilon, side_length=synthetic_side_length, noisy_side_length=noisy_side_length,
        central_position=central_position, validationSet_no_noise_df=None)

    scaled_noisy_training_set_df, scaled_noisy_test_set_df, scaled_adv_training_set_df, scaled_adv_test_set_df = \
        normalize_train_and_test_set.normalize_train_and_test_set(training_set_input_mat=noisy_training_set_mat,
                                                                  test_set_input_mat=noisy_test_set_mat,
                                                                  noisy_min_lat=noisy_min_lat,
                                                                  noisy_max_lat=noisy_max_lat,
                                                                  noisy_min_lon=noisy_min_lon,
                                                                  noisy_max_lon=noisy_max_lon,
                                                                  colNames=colNames,
                                                                  adv_training_set_mat=adv_training_set_mat,
                                                                  adv_test_set_mat=adv_test_set_mat,
                                                                  validation_set_input_mat=None,
                                                                  adv_validation_set_mat=None)
    # print "////////////////////////////"
    # print scaled_noisy_training_set_df
    # print scaled_noisy_test_set_df
    # print scaled_adv_training_set_df
    # print scaled_adv_test_set_df

    with open(sys.argv[1] + '/synthetic_dataset_experiments/synthetic_training_set_standalone_classifier_input',
              'wb') as f:
        pickle.dump(scaled_adv_training_set_df, f)

    with open(sys.argv[1] + '/synthetic_dataset_experiments/synthetic_test_set_standalone_classifier_input', 'wb') as f:
        pickle.dump(scaled_adv_test_set_df, f)

    #   create noisy datasets for experiments involving noise
    noisy_training_set_mat, noisy_test_set_mat, adv_training_set_mat, adv_test_set_mat, noisy_min_lat, \
    noisy_min_lon, noisy_max_lat, noisy_max_lon, colNames = laplacian_noise_injection.noiseInjection(
        trainingSet_no_noise_df=trainingSet_no_noise_df,
        testSet_no_noise_df=testSet_no_noise_df,
        repetitions=100,
        epsilon=epsilon, side_length=synthetic_side_length, noisy_side_length=noisy_side_length,
        central_position=central_position, validationSet_no_noise_df=None)

    scaled_noisy_training_set_df, scaled_noisy_test_set_df, scaled_adv_training_set_df, scaled_adv_test_set_df = \
        normalize_train_and_test_set.normalize_train_and_test_set(training_set_input_mat=noisy_training_set_mat,
                                                                  test_set_input_mat=noisy_test_set_mat,
                                                                  noisy_min_lat=noisy_min_lat,
                                                                  noisy_max_lat=noisy_max_lat,
                                                                  noisy_min_lon=noisy_min_lon,
                                                                  noisy_max_lon=noisy_max_lon,
                                                                  colNames=colNames,
                                                                  adv_training_set_mat=adv_training_set_mat,
                                                                  adv_test_set_mat=adv_test_set_mat,
                                                                  validation_set_input_mat=None,
                                                                  adv_validation_set_mat=None)
    # print "////////////////////////////"
    # print scaled_noisy_training_set_df
    # print scaled_noisy_test_set_df
    # print scaled_adv_training_set_df
    # print scaled_adv_test_set_df

    with open(sys.argv[1] + '/synthetic_dataset_experiments/synthetic_training_set_standalone_classifier_NOISY_input',
              'wb') as f:
        pickle.dump(scaled_noisy_training_set_df, f)

    with open(sys.argv[1] + '/synthetic_dataset_experiments/synthetic_test_set_standalone_classifier_NOISY_input',
              'wb') as f:
        pickle.dump(scaled_noisy_test_set_df, f)

    with open(sys.argv[1] + '/synthetic_dataset_experiments/synthetic_training_set_standalone_ADV_input', 'wb') as f:
        pickle.dump(scaled_adv_training_set_df, f)

    with open(sys.argv[1] + '/synthetic_dataset_experiments/synthetic_test_set_standalone_ADV_input', 'wb') as f:
        pickle.dump(scaled_adv_test_set_df, f)

    # sys.exit("END CREATION SYNTHETIC DATASET")

    ########################################################################################################################
    ##################################  EXTRACT ORIGINAL DATASET FROM .TXT AND  ############################################
    #################################################  SAVE IT  ############################################################
    ########################################################################################################################

    createDataset_from_txt.createDataset_fromTXT(inputFile=original_dataset_txt,
                                                 original_colNames=original_colnames,
                                                 save_df_file=original_dataset_df,
                                                 colsToDrop=colsToDrop)

    #   check if it worked
    loaded_obj = loadDataset.loadDataset(inputFile=original_dataset_df)
    print loaded_obj

    ########################################################################################################################
    ####################################  GET USERS OF INTEREST CREATE A DF AND  ###########################################
    #################################################  SAVE IT  ############################################################
    ########################################################################################################################

    get_users_of_interest.get_users_of_interest(
        inputDataset=original_dataset_df,
        central_position=central_position,
        side_length=side_length,
        usersOI_card=usersOI_card,
        users_of_interest_most_frequent_check_in_no_scaling_original_positions_df=
        users_of_interest_most_frequent_check_in_no_scaling_original_positions_df)

    # loaded_obj = loadDataset.loadDataset(
    #   inputFile=users_of_interest_most_frequent_check_in_no_scaling_original_positions_df)
    # print loaded_obj[["userID", "latitude", "longitude", "position_id"]]
    # print loaded_obj.drop_duplicates(["userID", "latitude", "longitude"])
    # print loaded_obj.shape
    # print len(np.where(loaded_obj.values[:, 0] == 39199)[0])

    ########################################################################################################################
    ####################################  SPLIT DATASET OF INTERESTING USERS FOR  ##########################################
    ####################################################  ML  ##############################################################
    ########################################################################################################################

    #   split training and test set
    loaded_obj_users_of_interest_most_frequent_check_in_no_scaling_original_positions_df = loadDataset.loadDataset(
        inputFile=users_of_interest_most_frequent_check_in_no_scaling_original_positions_df)

    split_dataset_for_ML.datasetSplit(datasetToSplit=
                                      loaded_obj_users_of_interest_most_frequent_check_in_no_scaling_original_positions_df,
                                      testPercentage=20)

    training_set_original_data_no_scaling_no_noise = loadDataset.loadDataset(
        inputFile=sys.argv[2] + "/PAN_datasets/training_set_original_data_no_scaling_no_noise")

    test_set_original_data_no_scaling_no_noise = loadDataset.loadDataset(
        inputFile=sys.argv[2] + "/PAN_datasets/test_set_original_data_no_scaling_no_noise")
    limited_area = create_square_limit(central_position=central_position, side_length=side_length)

    #   limits for not noisy data
    min_lat = limited_area[0]
    max_lat = limited_area[1]
    min_lon = limited_area[2]
    max_lon = limited_area[3]
    # print "min lat is: ", min_lat
    # print "min lon is: ", min_lon
    # print "max lat is: ", max_lat
    # print "max lon is: ", max_lon

    if min(training_set_original_data_no_scaling_no_noise.values[:, 1]) < min_lat:
        sys.exit("ERRORE MIN LAT")
    if max(training_set_original_data_no_scaling_no_noise.values[:, 1]) > max_lat:
        sys.exit("ERRORE MAX LAT")
    if min(training_set_original_data_no_scaling_no_noise.values[:, 2]) < min_lon:
        sys.exit("ERRORE MIN LON")
    if max(training_set_original_data_no_scaling_no_noise.values[:, 2]) > max_lon:
        sys.exit("ERROR MAX LON")

    print len(np.unique(training_set_original_data_no_scaling_no_noise.values[:, 3]))
    print len(np.unique(test_set_original_data_no_scaling_no_noise.values[:, 3]))

    ########################################################################################################################
    ###############################  ADD NOISE AND SCALE BUT NO REPETITIONS: THIS IS   #####################################
    ######################################  TO USE WITH NO NOISE DISCRIMINATOR  ############################################
    ########################################################################################################################

    training_set_original_data_no_scaling_no_noise = loadDataset.loadDataset(
        inputFile=sys.argv[2] + "/PAN_datasets/training_set_original_data_no_scaling_no_noise")


    test_set_original_data_no_scaling_no_noise = loadDataset.loadDataset(
        inputFile=sys.argv[2] + "/PAN_datasets_2/test_set_original_data_no_scaling_no_noise")

    noisy_training_set_mat_no_repetitions, \
    noisy_test_set_mat_no_repetitions, \
    adv_training_set_mat_no_repetitions, \
    adv_test_set_mat_no_repetitions, \
    noisy_min_lat, \
    noisy_min_lon, \
    noisy_max_lat, \
    noisy_max_lon, \
    colNames = laplacian_noise_injection.noiseInjection(
        trainingSet_no_noise_df=training_set_original_data_no_scaling_no_noise,
        testSet_no_noise_df=test_set_original_data_no_scaling_no_noise,
        repetitions=1,
        epsilon=epsilon,
        side_length=side_length,
        noisy_side_length=noisy_side_length,
        central_position=central_position)

    # print "###############################################################################################################"
    # print pn.DataFrame(data=noisy_training_set_mat_no_repetitions).shape
    # print pn.DataFrame(data=noisy_test_set_mat_no_repetitions).shape
    # print pn.DataFrame(data=adv_training_set_mat_no_repetitions).shape
    # print pn.DataFrame(data=adv_test_set_mat_no_repetitions).shape

    # print "###############################################################################################################"
    limited_area = create_square_limit(central_position=central_position, side_length=side_length)

    #   limits for not noisy data
    min_lat = limited_area[0]
    max_lat = limited_area[1]
    min_lon = limited_area[2]
    max_lon = limited_area[3]

    if min(adv_training_set_mat_no_repetitions[:, 0]) < min_lat:
        sys.exit("ERRORE MIN LAT")
    if max(adv_training_set_mat_no_repetitions[:, 0]) > max_lat:
        sys.exit("ERRORE MAX LAT")
    if min(adv_training_set_mat_no_repetitions[:, 1]) < min_lon:
        sys.exit("ERRORE MIN LON")
    if max(adv_training_set_mat_no_repetitions[:, 1]) > max_lon:
        sys.exit("ERROR MAX LON")

    """
    print "################################################################################################################"
    
    for i in range(0, len(noisy_test_set_mat_no_repetitions[:, 0])):
        if noisy_test_set_mat_no_repetitions[i, 0] != adv_test_set_mat_no_repetitions[i, 6]:
            sys.exit("a.values[:, 0] != c.values[:, 6]")
    for i in range(0, len(noisy_test_set_mat_no_repetitions[:, 0])):
        if noisy_test_set_mat_no_repetitions[i, 1] != adv_test_set_mat_no_repetitions[i, 4]:
            sys.exit("a.values[:, 1] != c.values[:, 4]")
    for i in range(0, len(noisy_test_set_mat_no_repetitions[:, 0])):
        if noisy_test_set_mat_no_repetitions[i, 2] != adv_test_set_mat_no_repetitions[i, 5]:
            sys.exit("a.values[:, 2] != c.values[:, 5]")
    for i in range(0, len(noisy_test_set_mat_no_repetitions[:, 0])):
        if noisy_test_set_mat_no_repetitions[i, 3] != adv_test_set_mat_no_repetitions[i, 7]:
            sys.exit("a.values[:, 3] != c.values[:, 7]")
    """

    scaled_noisy_training_set_df, \
    scaled_noisy_test_set_df, \
    scaled_adv_training_set_df, \
    scaled_adv_test_set_df = normalize_train_and_test_set.normalize_train_and_test_set(
        training_set_input_mat=noisy_training_set_mat_no_repetitions,
        test_set_input_mat=noisy_test_set_mat_no_repetitions,
        noisy_min_lat=noisy_min_lat,
        noisy_max_lat=noisy_max_lat,
        noisy_min_lon=noisy_min_lon,
        noisy_max_lon=noisy_max_lon,
        colNames=colNames,
        adv_training_set_mat=adv_training_set_mat_no_repetitions,
        adv_test_set_mat=adv_test_set_mat_no_repetitions)

    for i in range(0, len(training_set_original_data_no_scaling_no_noise.values[:, 0])):
        if training_set_original_data_no_scaling_no_noise.values[i, 3] != scaled_noisy_training_set_df.values[i, 3]:
            sys.exit("ERROR")
        if training_set_original_data_no_scaling_no_noise.values[i, 3] != scaled_adv_training_set_df.values[i, 7]:
            print training_set_original_data_no_scaling_no_noise.values[i, 3]
            print scaled_adv_training_set_df.values[i, 7]
            sys.exit("ERROR")

    with open(sys.argv[2] + "/PAN_datasets/discriminator_original_data_no_repetition/"
                            "scaled_noisy_training_set_df", 'wb') as f:
        pickle.dump(scaled_noisy_training_set_df, f)

    with open(sys.argv[2] + "/PAN_datasets/discriminator_original_data_no_repetition/"
                            "scaled_noisy_test_set_df", 'wb') as f:
        pickle.dump(scaled_noisy_test_set_df, f)

    with open(sys.argv[2] + "/PAN_datasets/discriminator_original_data_no_repetition/"
                            "scaled_adv_training_set_df", 'wb') as f:
        pickle.dump(scaled_adv_training_set_df, f)

    with open(sys.argv[2] + "/PAN_datasets/discriminator_original_data_no_repetition/"
                            "scaled_adv_test_set_df", 'wb') as f:
        pickle.dump(scaled_adv_test_set_df, f)

    ########################################################################################################################
    #################################  ADD NOISE AND SCALE WITH REPETITIONS: THIS IS   #####################################
    ####################################  TO USE WITH NOISE DISCRIMINATOR AND ADV ##########################################
    ########################################################################################################################

    training_set_original_data_no_scaling_no_noise = loadDataset.loadDataset(
        inputFile=sys.argv[2] + "/PAN_datasets/training_set_original_data_no_scaling_no_noise")

    test_set_original_data_no_scaling_no_noise = loadDataset.loadDataset(
        inputFile=sys.argv[2] + "/PAN_datasets_2/test_set_original_data_no_scaling_no_noise")

    noisy_training_set_mat_with_repetitions, \
    noisy_test_set_mat_with_repetitions, \
    adv_training_set_mat_with_repetitions, \
    adv_test_set_mat_with_repetitions, \
    noisy_min_lat, \
    noisy_min_lon, \
    noisy_max_lat, \
    noisy_max_lon, \
    colNames = laplacian_noise_injection.noiseInjection(
        trainingSet_no_noise_df=training_set_original_data_no_scaling_no_noise,
        testSet_no_noise_df=test_set_original_data_no_scaling_no_noise,
        repetitions=100,
        epsilon=epsilon,
        side_length=side_length,
        noisy_side_length=noisy_side_length,
        central_position=central_position)

    print noisy_training_set_mat_with_repetitions.shape
    # print noisy_validation_set_mat_with_repetitions.shape
    print noisy_test_set_mat_with_repetitions.shape
    print adv_training_set_mat_with_repetitions.shape
    # print adv_validation_set_mat_with_repetitions.shape
    print adv_test_set_mat_with_repetitions.shape

    print (noisy_training_set_mat_with_repetitions[0, 1])

    scaled_noisy_training_set_df, \
    scaled_noisy_test_set_df, \
    scaled_adv_training_set_df, \
    scaled_adv_test_set_df = normalize_train_and_test_set.normalize_train_and_test_set(
        training_set_input_mat=noisy_training_set_mat_with_repetitions,
        test_set_input_mat=noisy_test_set_mat_with_repetitions,
        noisy_min_lat=noisy_min_lat,
        noisy_max_lat=noisy_max_lat,
        noisy_min_lon=noisy_min_lon,
        noisy_max_lon=noisy_max_lon,
        colNames=colNames,
        adv_training_set_mat=adv_training_set_mat_with_repetitions,
        adv_test_set_mat=adv_test_set_mat_with_repetitions)

    with open(sys.argv[2] + "/PAN_datasets_2/data_with_repetition_for_discriminator_and_adv/"
                            "scaled_noisy_training_set_df", 'wb') as f:
        pickle.dump(scaled_noisy_training_set_df, f)

    with open(sys.argv[2] + "/PAN_datasets/data_with_repetition_for_discriminator_and_adv/"
                            "scaled_noisy_test_set_df", 'wb') as f:
        pickle.dump(scaled_noisy_test_set_df, f)

    with open(sys.argv[2] + "/PAN_datasets/data_with_repetition_for_discriminator_and_adv/"
                            "scaled_adv_training_set_df", 'wb') as f:
        pickle.dump(scaled_adv_training_set_df, f)

    with open(sys.argv[2] + "/PAN_datasets/data_with_repetition_for_discriminator_and_adv/"
                            "scaled_adv_test_set_df", 'wb') as f:
        pickle.dump(scaled_adv_test_set_df, f)


if __name__ == "__main__":
    main_func()
