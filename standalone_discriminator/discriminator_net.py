from keras.models import load_model
import os
import tensorflow as tf
import keras.backend as K
from keras import Input, Model
from keras.layers import Dense
from loadDataset import *
import pandas as pn
import operator
from keras import utils
import numpy as np
import sys
import random
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix


def compute_accuracy(y_classes, y_pred_classes):
    a = 0
    b = 0

    #print 'y_classes', y_classes
    #print 'y_pred_classes', y_pred_classes

    for i in range(0, len(y_pred_classes)):
        if y_pred_classes[i] == y_classes[i]:
            a += 1
        b += 1

    return a / float(b)


def compute_precision(y_classes, y_pred_classes):
    return precision_score(y_true=y_classes, y_pred=y_pred_classes, average=None)


def compute_recall(y_classes, y_pred_classes):
    return recall_score(y_true=y_classes, y_pred=y_pred_classes, average=None)


def compute_f1_score(y_classes, y_pred_classes):
    return f1_score(y_true=y_classes, y_pred=y_pred_classes, average="macro")


def compute_confusion_matric(y_classes, y_pred_classes):
    return confusion_matrix(y_true=y_classes, y_pred=y_pred_classes)


####################################################  MI computation  ##################################################
####################################################  MI computation  ##################################################
def condition(i, number_of_elements, y_true, y_pred, M):
    return i < number_of_elements


def condition2(i, number_of_elements, Mx, Mx_tmp):
    return i < number_of_elements


def body(i, number_of_elements, y_true, y_pred, M):
    tmp = tf.matmul(tf.gather(y_pred, i, axis=0), tf.transpose(tf.gather(y_true, i, axis=0)))
    tmp = tf.reshape(tmp, shape=[1, tf.shape(y_true)[1], tf.shape(y_true)[1]])
    M = tf.concat([M, tmp], axis=0)
    return i + 1, number_of_elements, y_true, y_pred, M


def body2(i, number_of_elements, Mx, Mx_tmp):
    Mx = tf.concat([Mx, Mx_tmp], axis=0)
    return i + 1, number_of_elements, Mx, Mx_tmp


def condition_H_true(iter, H_true, P_marg_true):
    return iter < tf.shape(P_marg_true)[0]


def body_H_true(iter, H_true, P_marg_true):
    #fix = tf.constant(0.0001)
    #p_x = tf.gather(P_marg_true, iter, axis=0)
    #a = tf.to_float(fix - (fix * p_x) + p_x)
    #b = tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))
    #H_true -= tf.to_float(a) * (tf.log(tf.to_float(a)) / b)

    p_x = tf.gather(P_marg_true, iter, axis=0)
    a = tf.to_float(p_x)
    b = tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))

    def f1():
        return tf.constant([0.0])

    def f2():
        return tf.to_float(a) * (tf.log(tf.to_float(a)) / b)

    H_true -= tf.cond(a[0] > 0.0, f2, f1)

    return iter + 1, H_true, P_marg_true


def condition_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    return jter < tf.shape(P_marg_true)[0]


def condition_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    return kter < tf.shape(P_marg_pred)[0]


def body_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    kter = tf.constant(0)
    tmp = tf.constant([0.0])

    jter, kter, tmp, P_marg_pred, P_jk = tf.while_loop(condition_H_true_cond_pred_2,
                                                       body_H_true_cond_pred_2,
                                                       [jter, kter, tmp, P_marg_pred, P_jk])

    H_true_cond_pred += tmp
    return jter + 1, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk


def body_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    #fix = tf.constant(0.0001)
    #a = fix - (fix * P_jk[kter, jter]) + P_jk[kter, jter]
    #b = fix - (fix * P_marg_pred[kter]) + P_marg_pred[kter]
    #tmp += -(a * (tf.log(a / b) / tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))))

    a = tf.to_float(P_jk[kter, jter])
    b = tf.to_float(P_marg_pred[kter])

    def f3():
        return tf.constant([0.0])

    def f4():
        return -(a * (tf.log(a / b) / tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))))

    tmp += tf.cond(a > 0.0, f4, f3)

    return jter, kter + 1, tmp, P_marg_pred, P_jk

def MI_loss(y_true, y_pred):
    # return K.categorical_crossentropy(y_true, y_pred)

    rank = len(y_pred.shape)
    axis = -1 % rank
    y_pred = y_pred / tf.reduce_sum(y_pred, axis, True)

    y_true = tf.reshape(y_true, [tf.shape(y_true)[0], tf.shape(y_true)[1], 1])
    y_pred = tf.reshape(y_pred, [tf.shape(y_pred)[0], tf.shape(y_pred)[1], 1])

    # y_true = tf.Print(y_true, [y_true], message="y_true ---> ", summarize=10)
    number_of_elements = tf.shape(y_true)[0]
    number_of_elements = tf.Print(number_of_elements,
                                  [number_of_elements],
                                  message="number_of_elements ---> ", summarize=10)

    i = tf.constant(1)
    M = tf.matmul(tf.gather(y_pred, 0, axis=0), tf.transpose(tf.gather(y_true, 0, axis=0)))
    M = tf.reshape(M, shape=[1, 4, 4])

    i, number_of_elements, y_true, y_pred, M = tf.while_loop(
        cond=condition,
        body=body,
        loop_vars=[i, number_of_elements, y_true, y_pred, M],
        shape_invariants=[i.get_shape(),
                          number_of_elements.get_shape(),
                          y_true.get_shape(),
                          y_pred.get_shape(),
                          tf.TensorShape([None, M.get_shape().as_list()[1], M.get_shape().as_list()[2]])])
    # tf.TensorShape(None)])
    # tf.TensorShape([None, 4, 4])])

    F_jk = tf.reduce_sum(M, axis=0)
    P_jk = tf.to_float(F_jk) / tf.to_float(number_of_elements)

    P_marg_true = tf.to_float(tf.reduce_sum(y_true, axis=0)) / tf.to_float(number_of_elements)

    P_marg_pred = tf.to_float(tf.reduce_sum(y_pred, axis=0)) / tf.to_float(number_of_elements)

    iter = tf.constant(0)
    H_true = tf.constant([0.0])

    iter, H_true, P_marg_true = tf.while_loop(condition_H_true, body_H_true, [iter, H_true, P_marg_true])

    H_true = tf.Print(H_true, [H_true], message="H_true")

    H_true_cond_pred = tf.constant([0.0])
    jter = tf.constant(0)

    jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk = tf.while_loop(condition_H_true_cond_pred,
                                                                           body_H_true_cond_pred,
                                                                           [jter,
                                                                            H_true_cond_pred,
                                                                            P_marg_pred,
                                                                            P_marg_true,
                                                                            P_jk])

    H_true_cond_pred = tf.Print(H_true_cond_pred, [H_true_cond_pred], message="H_true_cond_pred")

    MI = H_true - H_true_cond_pred

    return -MI


########################################################################################################################
def setGPU(GPU_index, memory_percentage):
    #   only use GPU with index 0, index must be a string
    os.environ["CUDA_VISIBLE_DEVICES"] = GPU_index

    # TensorFlow wizardry
    config = tf.ConfigProto()

    # Don't pre-allocate memory; allocate as-needed
    config.gpu_options.allow_growth = True

    # Only allow a total of half the GPU memory to be allocated
    config.gpu_options.per_process_gpu_memory_fraction = memory_percentage

    # Create a session with the above options specified.
    K.tensorflow_backend.set_session(tf.Session(config=config))


def build_classifier():
    hiddenLayersCard = 3
    hiddenNeuronsCard = [60, 100, 51, 66, 49, 65, 61, 49, 63, 100]
    # inputs of 2D vectors
    input_layer = Input(shape=(2,), name='discriminator_input')
    # at least one hidden layer
    hidden_layer = Dense(hiddenNeuronsCard[0], activation='relu', name='hidden_layer_0')(input_layer)
    # other hidden layers if any
    for idx in range(1, hiddenLayersCard):
        layer_name = 'hidden_layer_' + str(idx)
        hidden_layer = Dense(hiddenNeuronsCard[idx], activation='relu', name=layer_name)(hidden_layer)
    # output layer
    output_layer = Dense(4, activation='softmax', name='output_layer')(hidden_layer)

    #   create the network model
    discriminator = Model(inputs=input_layer, outputs=output_layer, name='discriminatorANN')

    # parallel_model = multi_gpu_model(discriminator, gpus=2)
    # parallel_model.compile(loss='categorical_crossentropy',
    #                       optimizer='adam',
    #                       metrics=['categorical_accuracy'])

    discriminator.compile(loss='categorical_crossentropy',
                          optimizer='adam',
                          metrics=['categorical_accuracy'])

    """
    discriminator.compile(loss=MI_loss, optimizer='adam')
    """
    # print(parallel_model.summary())
    print discriminator.summary()

    return discriminator


def datasetSplit(supervisionDiscriminator_mat, testPercentage, valPercentage):
    #   compute number of rows and cols for the dataseToSplit
    numRow_datasetToSplit_matrix = supervisionDiscriminator_mat.shape[0]
    # print "numRow_datasetToSplit_matrix ---> ", numRow_datasetToSplit_matrix
    numCol_datasetToSplit_matrix = supervisionDiscriminator_mat.shape[1]
    # print "numCol_datasetToSplit_matrix ---> ", numCol_datasetToSplit_matrix

    #   prepare list of list for training set
    trainingSet_list = []

    #   prepare list of list for validation set
    validationSet_list = []

    #   prepare list of list for test set
    testSet_list = []

    #   list of unique user ids
    unique_user_ids_list = np.unique(ar=supervisionDiscriminator_mat[:, 0])
    # print "unique_user_ids_list ---> ", unique_user_ids_list

    #   loop over ids
    for i in unique_user_ids_list:
        #   check in which positions every id is: eaning the index in the dataset matrix
        where_id_in_area_of_interest_most_freq_users_original_dataset_df = \
            np.where(supervisionDiscriminator_mat[:, 0] == i)[0]
        #   shuffle the ids order
        np.random.shuffle(where_id_in_area_of_interest_most_freq_users_original_dataset_df)
        # print where_id_in_area_of_interest_most_freq_users_original_dataset_df
        #   split into training and test set

        #   compute number of elements in each set
        testCard_current_id = testPercentage * \
                              len(where_id_in_area_of_interest_most_freq_users_original_dataset_df) // 100
        # print "testCard ---> ", testCard
        trainCard_current_id = len(where_id_in_area_of_interest_most_freq_users_original_dataset_df) - \
                               testCard_current_id
        # print "trainCard ---> ", trainCard
        valCard_current_id = valPercentage * trainCard_current_id // 100

        trainCard_current_id = trainCard_current_id - valCard_current_id

        #   push elements ids in the matrices of training and test set
        for j in range(0, trainCard_current_id):
            trainingSet_list.append(where_id_in_area_of_interest_most_freq_users_original_dataset_df[j])

        for k in range(trainCard_current_id, trainCard_current_id + testCard_current_id):
            testSet_list.append(where_id_in_area_of_interest_most_freq_users_original_dataset_df[k])

        for w in range(trainCard_current_id + testCard_current_id,
                       len(where_id_in_area_of_interest_most_freq_users_original_dataset_df)):
            validationSet_list.append(where_id_in_area_of_interest_most_freq_users_original_dataset_df[w])

    # print "np.array(trainingSet_list)\n", np.array(trainingSet_list)
    # print "np.array(testSet_list)\n", np.array(testSet_list)

    # print len(np.where(np.array(trainingSet_list)[:, 0] == datasetToSplit_matrix[736, 0])[0])
    # print len(np.where(np.array(testSet_list)[:, 0] == datasetToSplit_matrix[736, 0])[0])

    # trainingSet_no_noise_df = pn.DataFrame(data=np.array(trainingSet_list), columns=supervisionDiscriminator_mat.columns.values)
    # testSet_no_noise_df = pn.DataFrame(data=np.array(testSet_list), columns=supervisionDiscriminator_mat.columns.values)

    # print "trainingSet_no_noise_df", trainingSet_no_noise_df
    # print "testSet_no_noise_df", testSet_no_noise_df

    random.shuffle(validationSet_list)
    with open(result_folder + 'datasets/index_in_train_for_validation.pkl', 'wb') as f:
        pickle.dump(validationSet_list, f)
    return [trainingSet_list, testSet_list, validationSet_list]


#   one hot coding for discriminator output
#   weight computation for dataset classification balancing
def one_hot_coding_and_weight_computation(supervision):
    #   one-hot coding user_id

    #   get unique user_id
    supervisionUnique, counts = np.unique(supervision, return_counts=True)
    #   transform the result of the previous operation in a python dict so we can sort it
    #   zip associates id with number of occurrences
    #   then a dict gets built
    dict_ = dict(zip(supervisionUnique, counts))
    # print dict_
    #   sorting the value of unique in order of increasing id
    sorted_x = sorted(dict_.items(), key=operator.itemgetter(0), )
    # print "sorted_x ---> ", sorted_x

    #   max occurrence
    max_occurrence = -float('inf')
    for i in range(0, len(sorted_x)):
        if sorted_x[i][1] > max_occurrence:
            max_occurrence = sorted_x[i][1]
    #   print max_occurrence

    weight_classes_dictionary = {}
    for j in range(0, len(sorted_x)):
        #   the weight for a class is the number of occurrences of the most frequent class over the occurrences of the
        #   class whose weight I am computing
        weight_classes_dictionary[j] = max_occurrence / float(sorted_x[j][1])
    # print "weight_classes_dictionary", weight_classes_dictionary
    #   change all user_id to their unique coding version (from 0 to len(unique(supervision))-1)
    oldClass_newClass_dict = {}
    for i in range(0, len(supervision)):
        for j in range(0, len(sorted_x)):
            if supervision[i] == sorted_x[j][0]:
                oldClass_newClass_dict[j] = supervision[i]
                supervision[i] = j
    # print "oldClass_newClass_dict ---> ", oldClass_newClass_dict
    #   one-hot coding of supervision
    supervision = utils.to_categorical(supervision)
    cont = 0
    for i in range(0, len(supervision)):
        if supervision[i][2] == 1:
            cont += 1
    # print cont
    # print len(supervision)
    return [supervision, weight_classes_dictionary]


#   to run this function as standalone
if __name__ == "__main__":

    setGPU(sys.argv[3], float(sys.argv[4]))

    training_set_file = sys.argv[5]
    test_set_file = sys.argv[6]

    if str(sys.argv[1]) == "all":
        # batch_size = training_set_mat.shape[0]
        batch_size = "all"
    else:
        batch_size = int(sys.argv[1])

    number_of_epochs = int(sys.argv[2])

    result_folder = sys.argv[7] + "_bs_" + str(batch_size) + \
                    "_noe_" + str(number_of_epochs) + "/"

    if not os.path.isdir(result_folder):
        os.makedirs(result_folder)
        os.makedirs(result_folder + "datasets/")

    no_repetitions_scaled_adv_training_set_df = loadDataset(inputFile=training_set_file)
    no_repetitions_scaled_adv_test_set_df = loadDataset(inputFile=test_set_file)

    # print no_repetitions_scaled_adv_training_set_df
    # print no_repetitions_scaled_adv_test_set_df

    userId_col = int(sys.argv[8])
    col_lat = int(sys.argv[9])
    col_lon = int(sys.argv[10])

    training_supervision_df = pn.DataFrame(data=no_repetitions_scaled_adv_training_set_df.values[:, userId_col],
                                           columns=["id"])
    training_supervision_mat = training_supervision_df.values

    test_supervision_df = pn.DataFrame(data=no_repetitions_scaled_adv_test_set_df.values[:, userId_col],
                                       columns=["id"])
    test_supervision_mat = test_supervision_df.values

    no_repetitions_scaled_adv_training_set_df = pn.DataFrame(
        data=no_repetitions_scaled_adv_training_set_df.values[:, col_lat:col_lon+1], columns=["scaled_orig_lat", "scaled_orig_lon"])
    no_repetitions_scaled_adv_training_set_mat = no_repetitions_scaled_adv_training_set_df.values

    no_repetitions_scaled_adv_test_set_df = pn.DataFrame(
        data=no_repetitions_scaled_adv_test_set_df.values[:, col_lat:col_lon+1], columns=["scaled_orig_lat", "scaled_orig_lon"])
    no_repetitions_scaled_adv_test_set_mat = no_repetitions_scaled_adv_test_set_df.values

    training_idx, test_idx, val_idx = datasetSplit(supervisionDiscriminator_mat=training_supervision_mat,
                                                   testPercentage=0, valPercentage=10)

    training_set_mat = no_repetitions_scaled_adv_training_set_mat[training_idx, :]
    print "training_set_shape ---> ", training_set_mat.shape
    validation_set_mat = no_repetitions_scaled_adv_training_set_mat[val_idx, :]
    print "validation_set_shape ---> ", validation_set_mat.shape
    test_set_mat = no_repetitions_scaled_adv_test_set_mat
    print "test_set_shape ---> ", test_set_mat.shape

    train_superv_mat, weight_train = one_hot_coding_and_weight_computation(training_supervision_mat[training_idx, :])
    print "train_supervision_shape ---> ", train_superv_mat.shape
    val_superv_mat = one_hot_coding_and_weight_computation(training_supervision_mat[val_idx, :])[0]
    print "val_supervision_shape ---> ", val_superv_mat.shape
    test_superv_mat = one_hot_coding_and_weight_computation(test_supervision_mat)[0]
    print "test_supervision_shape ---> ", test_superv_mat.shape

    with open(result_folder + 'datasets/train_mat', 'wb') as f:
        pickle.dump(training_set_mat, f)

    with open(result_folder + 'datasets/val_mat', 'wb') as f:
        pickle.dump(validation_set_mat, f)

    with open(result_folder + 'datasets/test_mat', 'wb') as f:
        pickle.dump(test_set_mat, f)

    with open(result_folder + 'datasets/train_supervision', 'wb') as f:
        pickle.dump(train_superv_mat, f)

    with open(result_folder + 'datasets/val_supervision', 'wb') as f:
        pickle.dump(val_superv_mat, f)

    with open(result_folder + 'datasets/test_supervision', 'wb') as f:
        pickle.dump(test_superv_mat, f)

    with open(result_folder + 'datasets/class_weights', 'wb') as f:
        pickle.dump(weight_train, f)

    discriminator_epochs = []
    discriminator_loss_vec = []
    discriminator_categ_acc_vec = []
    discriminator_val_loss_vec = []
    discriminator_val_categ_acc_vec = []

    discriminator_model = load_model(sys.argv[11])

    if batch_size == "all":
        batch_size = training_set_mat.shape[0]

    for step in range(0, number_of_epochs):
        # discriminator_model.fit(x=training_set_mat, y=train_superv_mat, batch_size=2080, epochs=1, shuffle=True,
        #                        validation_data=(validation_set_mat, val_superv_mat))
        history_discriminator = discriminator_model.fit(x=training_set_mat,
                                                        y=train_superv_mat,
                                                        batch_size=batch_size,
                                                        epochs=1,
                                                        shuffle=True,
                                                        validation_data=(validation_set_mat, val_superv_mat))
        """
        # y_pred_prob = discriminator_model.predict(x=training_set_mat, batch_size=221)
        y_pred_prob = discriminator_model.predict(x=training_set_mat,
                                                  batch_size=training_set_mat.shape[0])
        y_pred_classes = y_pred_prob.argmax(axis=-1)
        y_prob = train_superv_mat
        y_classes = y_prob.argmax(axis=-1)
        """
        discriminator_epochs.append(len(history_discriminator.history.get('loss')))
        for i in range(0, len(history_discriminator.history.get('loss'))):
            discriminator_loss_vec.append(
                history_discriminator.history.get('loss')[i])
            discriminator_categ_acc_vec.append(
                history_discriminator.history.get('categorical_accuracy')[i])
            discriminator_val_loss_vec.append(
                history_discriminator.history.get('val_loss')[i])
            discriminator_val_categ_acc_vec.append(
                history_discriminator.history.get('val_categorical_accuracy')[i])

    #   save all vectors
    with open(result_folder + 'discriminator_epochs.pkl', 'wb') as f:
        pickle.dump(discriminator_epochs, f)
    with open(result_folder + 'discriminator_loss_vec.pkl', 'wb') as f:
        pickle.dump(discriminator_loss_vec, f)
    with open(result_folder + 'discriminator_categ_acc_vec.pkl', 'wb') as f:
        pickle.dump(discriminator_categ_acc_vec, f)
    with open(result_folder + 'discriminator_val_loss_vec.pkl', 'wb') as f:
        pickle.dump(discriminator_val_loss_vec, f)
    with open(result_folder + 'discriminator_val_categ_acc_vec.pkl', 'wb') as f:
        pickle.dump(discriminator_val_categ_acc_vec, f)

    # print discriminator_model.evaluate(x=test_set_mat, y=test_superv_mat, batch_size=569)
    evaluation_on_test = []
    evaluation_on_test.append(discriminator_model.evaluate(x=test_set_mat,
                                                           y=test_superv_mat,
                                                           batch_size=test_set_mat.shape[0]))
    print 'evaluation_on_test --- >', evaluation_on_test
    with open(result_folder + 'evaluation_on_test.pkl', 'wb') as f:
        pickle.dump(evaluation_on_test, f)

    #   prediction training set
    training_set_prediction = discriminator_model.predict(x=training_set_mat)
    training_set_prediction = training_set_prediction.argmax(axis=-1)
    print "confusion matrix on training set ---> ", compute_confusion_matric(y_classes=train_superv_mat.argmax(axis=-1),
                                                                             y_pred_classes=training_set_prediction)
    print "accuracy on training set ---> ", compute_accuracy(y_classes=train_superv_mat.argmax(axis=-1),
                                                             y_pred_classes=training_set_prediction)
    print "precision on training set ---> ", compute_precision(y_classes=train_superv_mat.argmax(axis=-1),
                                                               y_pred_classes=training_set_prediction)
    print "recall on training set ---> ", compute_recall(y_classes=train_superv_mat.argmax(axis=-1),
                                                         y_pred_classes=training_set_prediction)
    print "f1 score on training set ---> ", compute_f1_score(y_classes=train_superv_mat.argmax(axis=-1),
                                                             y_pred_classes=training_set_prediction)
    training_set_prediction_df_1 = pn.DataFrame(data=training_set_mat, columns=["lat", "long"])
    training_set_prediction_df_2 = pn.DataFrame(data=training_set_prediction, columns=["predicted_user_id"])
    training_set_prediction_df = pn.concat([training_set_prediction_df_1, training_set_prediction_df_2], axis=1)
    # print training_set_prediction_df
    with open(result_folder + 'training_set_prediction_df.pkl', 'wb') as f:
        pickle.dump(training_set_prediction_df, f)

    #   prediction validation set
    validation_set_prediction = discriminator_model.predict(x=validation_set_mat)
    validation_set_prediction = validation_set_prediction.argmax(axis=-1)
    print "confusion matrix on validation set ---> ", compute_confusion_matric(y_classes=val_superv_mat.argmax(axis=-1),
                                                                             y_pred_classes=validation_set_prediction)
    print "accuracy on validation set ---> ", compute_accuracy(y_classes=val_superv_mat.argmax(axis=-1),
                                                               y_pred_classes=validation_set_prediction)
    print "precision on validation set ---> ", compute_precision(y_classes=val_superv_mat.argmax(axis=-1),
                                                                 y_pred_classes=validation_set_prediction)
    print "recall on validation set ---> ", compute_recall(y_classes=val_superv_mat.argmax(axis=-1),
                                                           y_pred_classes=validation_set_prediction)
    print "f1 score on validation set ---> ", compute_f1_score(y_classes=val_superv_mat.argmax(axis=-1),
                                                               y_pred_classes=validation_set_prediction)
    validation_set_prediction_df_1 = pn.DataFrame(data=validation_set_mat, columns=["lat", "long"])
    validation_set_prediction_df_2 = pn.DataFrame(data=validation_set_prediction, columns=["predicted_user_id"])
    validation_set_prediction_df = pn.concat([validation_set_prediction_df_1, validation_set_prediction_df_2], axis=1)
    # print validation_set_prediction_df
    with open(result_folder + 'validation_set_prediction_df.pkl', 'wb') as f:
        pickle.dump(validation_set_prediction_df, f)

    #   prediction test set
    test_set_prediction = discriminator_model.predict(x=test_set_mat)
    test_set_prediction = test_set_prediction.argmax(axis=-1)
    print "confusion matrix on test set ---> ", compute_confusion_matric(y_classes=test_superv_mat.argmax(axis=-1),
                                                                             y_pred_classes=test_set_prediction)
    print "accuracy on test set ---> ", compute_accuracy(y_classes=test_superv_mat.argmax(axis=-1),
                                                         y_pred_classes=test_set_prediction)
    print "precision on test set ---> ", compute_precision(y_classes=test_superv_mat.argmax(axis=-1),
                                                           y_pred_classes=test_set_prediction)
    print "recall on test set ---> ", compute_recall(y_classes=test_superv_mat.argmax(axis=-1),
                                                     y_pred_classes=test_set_prediction)
    print "f1 score on test set ---> ", compute_f1_score(y_classes=test_superv_mat.argmax(axis=-1),
                                                         y_pred_classes=test_set_prediction)
    test_set_prediction_df_1 = pn.DataFrame(data=test_set_mat, columns=["lat", "long"])
    test_set_prediction_df_2 = pn.DataFrame(data=test_set_prediction, columns=["predicted_user_id"])
    test_set_prediction_df = pn.concat([test_set_prediction_df_1, test_set_prediction_df_2], axis=1)
    # print test_set_prediction_df
    with open(result_folder + 'test_set_prediction_df.pkl', 'wb') as f:
        pickle.dump(test_set_prediction_df, f)
