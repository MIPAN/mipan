
"""
this is the advNet version to use the same dataset used to train the discriminator
"""

"""
This is the adversarial network model consisting of a discriminator on top of a generator
"""
from loadDataset import *
from keras.models import load_model
from keras import Input, Model
import os
import tensorflow as tf
import numpy as np
import operator
from keras import utils
from keras import optimizers
from keras import callbacks as cb
from keras import backend as K
import pandas as pn
import sys
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix
import random


def compute_accuracy(y_classes, y_pred_classes):
    a = 0
    b = 0

    # print 'y_classes', y_classes
    # print 'y_pred_classes', y_pred_classes

    for i in range(0, len(y_pred_classes)):
        if y_pred_classes[i] == y_classes[i]:
            a += 1
        b += 1

    return a / float(b)


def compute_precision(y_classes, y_pred_classes):
    return precision_score(y_true=y_classes, y_pred=y_pred_classes, average=None)


def compute_recall(y_classes, y_pred_classes):
    return recall_score(y_true=y_classes, y_pred=y_pred_classes, average=None)


def compute_f1_score(y_classes, y_pred_classes):
    return f1_score(y_true=y_classes, y_pred=y_pred_classes, average="macro")


def compute_confusion_matric(y_classes, y_pred_classes):
    return confusion_matrix(y_true=y_classes, y_pred=y_pred_classes)


####################################################  MI computation  ##################################################
def condition(i, number_of_elements, y_true, y_pred, M):
    return i < number_of_elements


def condition2(i, number_of_elements, Mx, Mx_tmp):
    return i < number_of_elements


def body(i, number_of_elements, y_true, y_pred, M):
    tmp = tf.matmul(tf.gather(y_pred, i, axis=0), tf.transpose(tf.gather(y_true, i, axis=0)))
    tmp = tf.reshape(tmp, shape=[1, tf.shape(y_true)[1], tf.shape(y_true)[1]])
    M = tf.concat([M, tmp], axis=0)
    return i + 1, number_of_elements, y_true, y_pred, M


def body2(i, number_of_elements, Mx, Mx_tmp):
    Mx = tf.concat([Mx, Mx_tmp], axis=0)
    return i + 1, number_of_elements, Mx, Mx_tmp


def condition_H_true(iter, H_true, P_marg_true):
    return iter < tf.shape(P_marg_true)[0]


def body_H_true(iter, H_true, P_marg_true):
    #fix = tf.constant(0.0001)
    #p_x = tf.gather(P_marg_true, iter, axis=0)
    #a = tf.to_float(fix - (fix * p_x) + p_x)
    #b = tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))
    #H_true -= tf.to_float(a) * (tf.log(tf.to_float(a)) / b)

    p_x = tf.gather(P_marg_true, iter, axis=0)
    a = tf.to_float(p_x)
    b = tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))

    def f1():
        return tf.constant([0.0])

    def f2():
        return tf.to_float(a) * (tf.log(tf.to_float(a)) / b)

    H_true -= tf.cond(a[0] > 0.0, f2, f1)

    return iter + 1, H_true, P_marg_true


def condition_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    return jter < tf.shape(P_marg_true)[0]


def condition_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    return kter < tf.shape(P_marg_pred)[0]


def body_H_true_cond_pred(jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk):
    kter = tf.constant(0)
    tmp = tf.constant([0.0])

    jter, kter, tmp, P_marg_pred, P_jk = tf.while_loop(condition_H_true_cond_pred_2,
                                                       body_H_true_cond_pred_2,
                                                       [jter, kter, tmp, P_marg_pred, P_jk])

    H_true_cond_pred += tmp
    return jter + 1, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk


def body_H_true_cond_pred_2(jter, kter, tmp, P_marg_pred, P_jk):
    #fix = tf.constant(0.0001)
    #a = fix - (fix * P_jk[kter, jter]) + P_jk[kter, jter]
    #b = fix - (fix * P_marg_pred[kter]) + P_marg_pred[kter]
    #tmp += -(a * (tf.log(a / b) / tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))))

    a = tf.to_float(P_jk[kter, jter])
    b = tf.to_float(P_marg_pred[kter])

    def f3():
        return tf.constant([0.0])

    def f4():
        return -(a * (tf.log(a / b) / tf.log(tf.to_float(tf.constant(2, dtype=tf.to_float(a).dtype)))))

    tmp += tf.cond(a > 0.0, f4, f3)

    return jter, kter + 1, tmp, P_marg_pred, P_jk


def MI_loss(y_true, y_pred):
    # return K.categorical_crossentropy(y_true, y_pred)

    rank = len(y_pred.shape)
    axis = -1 % rank
    y_pred = y_pred / tf.reduce_sum(y_pred, axis, True)

    y_true = tf.reshape(y_true, [tf.shape(y_true)[0], tf.shape(y_true)[1], 1])
    y_pred = tf.reshape(y_pred, [tf.shape(y_pred)[0], tf.shape(y_pred)[1], 1])

    # y_true = tf.Print(y_true, [y_true], message="y_true ---> ", summarize=10)
    number_of_elements = tf.shape(y_true)[0]
    number_of_elements = tf.Print(number_of_elements,
                                  [number_of_elements],
                                  message="number_of_elements ---> ", summarize=10)

    i = tf.constant(1)
    M = tf.matmul(tf.gather(y_pred, 0, axis=0), tf.transpose(tf.gather(y_true, 0, axis=0)))
    #M = tf.reshape(M, shape=[1, 4, 4])
    M = tf.reshape(M, shape=[1, 6, 6])

    i, number_of_elements, y_true, y_pred, M = tf.while_loop(
        cond=condition,
        body=body,
        loop_vars=[i, number_of_elements, y_true, y_pred, M],
        shape_invariants=[i.get_shape(),
                          number_of_elements.get_shape(),
                          y_true.get_shape(),
                          y_pred.get_shape(),
                          tf.TensorShape([None, M.get_shape().as_list()[1], M.get_shape().as_list()[2]])])
    # tf.TensorShape(None)])
    # tf.TensorShape([None, 4, 4])])

    F_jk = tf.reduce_sum(M, axis=0)
    P_jk = tf.to_float(F_jk) / tf.to_float(number_of_elements)

    P_marg_true = tf.to_float(tf.reduce_sum(y_true, axis=0)) / tf.to_float(number_of_elements)

    P_marg_pred = tf.to_float(tf.reduce_sum(y_pred, axis=0)) / tf.to_float(number_of_elements)

    iter = tf.constant(0)
    H_true = tf.constant([0.0])

    iter, H_true, P_marg_true = tf.while_loop(condition_H_true, body_H_true, [iter, H_true, P_marg_true])

    H_true = tf.Print(H_true, [H_true], message="H_true")

    H_true_cond_pred = tf.constant([0.0])
    jter = tf.constant(0)

    jter, H_true_cond_pred, P_marg_pred, P_marg_true, P_jk = tf.while_loop(condition_H_true_cond_pred,
                                                                           body_H_true_cond_pred,
                                                                           [jter,
                                                                            H_true_cond_pred,
                                                                            P_marg_pred,
                                                                            P_marg_true,
                                                                            P_jk])

    H_true_cond_pred = tf.Print(H_true_cond_pred, [H_true_cond_pred], message="H_true_cond_pred")

    MI = H_true - H_true_cond_pred

    return MI


########################################################################################################################
#   new loss function, softplus centered in 280 so that we can compare the distance we obtain, i.e. euclid distance,
#   to this one
def adv_euclid_280_meters(y_true, y_pred):
    #   compute the mean squared error for the obtained value and supervision
    y_true = K.cast(y_true, dtype='float32')
    y_pred = K.cast(y_pred, dtype='float32')
    mse = tf.sqrt(tf.reduce_sum(tf.square(y_true - y_pred), reduction_indices=1))
    me = K.mean(mse)
    me = tf.scalar_mul(scalar=3250, x=me)
    me = me - 280
    # me = tf.scalar_mul(scalar=0.1, x=me)
    me = tf.nn.softplus(me)
    # me = tf.square(me)
    return me


#   setu for GPU usage
def GPU_setup(id_GPU, memory_percentage):
    #   only use GPU with index id_GPU
    #   id_GPU is a string like "0"
    os.environ["CUDA_VISIBLE_DEVICES"] = id_GPU

    #   TensorFlow wizardry
    config = tf.ConfigProto()

    #   Don't pre_allocate memory; allocate as_needed
    config.gpu_options.allow_growth = True

    #   Only allow a total of half the GPU memory to be allocated
    #   memory_percentage is a float between 0 and 1
    config.gpu_options.per_process_gpu_memory_fraction = memory_percentage

    #   Create a session with the above options specified.
    K.tensorflow_backend.set_session(tf.Session(config=config))


"""
#   define new loss function for adversarial to push the generator to decrease the accuracy of the discriminator, namely
#   min -crossentropy = max crossentropy
def opposite_categorical_crossentropy(y_true, y_pred):
    return -K.categorical_crossentropy(y_true, y_pred)
"""


#   split of the indexes for training and validation data
def datasetSplit(supervisionDiscriminator_mat, testPercentage, valPercentage):
    #   compute number of rows and cols for the dataseToSplit
    numRow_datasetToSplit_matrix = supervisionDiscriminator_mat.shape[0]
    # print "numRow_datasetToSplit_matrix ---> ", numRow_datasetToSplit_matrix
    numCol_datasetToSplit_matrix = supervisionDiscriminator_mat.shape[1]
    # print "numCol_datasetToSplit_matrix ---> ", numCol_datasetToSplit_matrix

    #   prepare list of list for training set
    trainingSet_list = []

    #   prepare list of list for validation set
    validationSet_list = []

    #   prepare list of list for test set
    testSet_list = []

    #   list of unique user ids
    unique_user_ids_list = np.unique(ar=supervisionDiscriminator_mat[:, 0])
    # print "unique_user_ids_list ---> ", unique_user_ids_list

    #   loop over ids
    for i in unique_user_ids_list:
        #   check in which positions every id is: eaning the index in the dataset matrix
        where_id_in_area_of_interest_most_freq_users_original_dataset_df = \
            np.where(supervisionDiscriminator_mat[:, 0] == i)[0]
        #   shuffle the ids order
        np.random.shuffle(where_id_in_area_of_interest_most_freq_users_original_dataset_df)
        # print where_id_in_area_of_interest_most_freq_users_original_dataset_df
        #   split into training and test set

        #   compute number of elements in each set
        testCard_current_id = testPercentage * \
                              len(where_id_in_area_of_interest_most_freq_users_original_dataset_df) // 100
        # print "testCard ---> ", testCard
        trainCard_current_id = len(where_id_in_area_of_interest_most_freq_users_original_dataset_df) - \
                               testCard_current_id
        # print "trainCard ---> ", trainCard
        valCard_current_id = valPercentage * trainCard_current_id // 100

        trainCard_current_id = trainCard_current_id - valCard_current_id

        #   push elements ids in the matrices of training and test set
        for j in range(0, trainCard_current_id):
            trainingSet_list.append(where_id_in_area_of_interest_most_freq_users_original_dataset_df[j])

        for k in range(trainCard_current_id, trainCard_current_id + testCard_current_id):
            testSet_list.append(where_id_in_area_of_interest_most_freq_users_original_dataset_df[k])

        for w in range(trainCard_current_id + testCard_current_id,
                       len(where_id_in_area_of_interest_most_freq_users_original_dataset_df)):
            validationSet_list.append(where_id_in_area_of_interest_most_freq_users_original_dataset_df[w])

    # print "np.array(trainingSet_list)\n", np.array(trainingSet_list)
    # print "np.array(testSet_list)\n", np.array(testSet_list)

    # print len(np.where(np.array(trainingSet_list)[:, 0] == datasetToSplit_matrix[736, 0])[0])
    # print len(np.where(np.array(testSet_list)[:, 0] == datasetToSplit_matrix[736, 0])[0])

    # trainingSet_no_noise_df = pn.DataFrame(data=np.array(trainingSet_list), columns=supervisionDiscriminator_mat.columns.values)
    # testSet_no_noise_df = pn.DataFrame(data=np.array(testSet_list), columns=supervisionDiscriminator_mat.columns.values)

    # print "trainingSet_no_noise_df", trainingSet_no_noise_df
    # print "testSet_no_noise_df", testSet_no_noise_df
    random.shuffle(validationSet_list)
    return [trainingSet_list, testSet_list, validationSet_list]


#   one hot coding for discriminator output
#   weight computation for dataset classification balancing
def one_hot_coding_and_weight_computation(supervision):
    #   one-hot coding user_id

    #   get unique user_id
    supervisionUnique, counts = np.unique(supervision, return_counts=True)
    #   transform the result of the previous operation in a python dict so we can sort it
    #   zip associates id with number of occurences
    #   then a dict gets built
    dict_ = dict(zip(supervisionUnique, counts))
    # print dict_
    #   sorting the value of unique in order of increasing id
    sorted_x = sorted(dict_.items(), key=operator.itemgetter(0), )
    # print "sorted_x ---> ", sorted_x

    #   max occurrence
    max_occurrence = -float('inf')
    for i in range(0, len(sorted_x)):
        if sorted_x[i][1] > max_occurrence:
            max_occurrence = sorted_x[i][1]
    #   print max_occurrence

    weight_classes_dictionary = {}
    for j in range(0, len(sorted_x)):
        #   the weight for a class is the number of occurrences of the most frequent class over the occurrences of the
        #   class whose weight I am computing
        weight_classes_dictionary[j] = max_occurrence / float(sorted_x[j][1])
    # print "weight_classes_dictionary", weight_classes_dictionary
    #   change all user_id to their unique coding version (from 0 to len(unique(supervision))-1)
    oldClass_newClass_dict = {}
    for i in range(0, len(supervision)):
        for j in range(0, len(sorted_x)):
            if supervision[i] == sorted_x[j][0]:
                oldClass_newClass_dict[j] = supervision[i]
                supervision[i] = j
    # print "oldClass_newClass_dict ---> ", oldClass_newClass_dict
    #   one-hot coding of supervision
    supervision = utils.to_categorical(supervision)
    cont = 0
    for i in range(0, len(supervision)):
        if supervision[i][2] == 1:
            cont += 1
    # print cont
    # print len(supervision)
    return [supervision, weight_classes_dictionary]


def adversarialModel(train, trainSupervision, test, testSpervision, discrimininator_model_file, generator_model_file,
                     id_GPU, memory_percentage, result_folder):
    GPU_setup(id_GPU=id_GPU, memory_percentage=memory_percentage)

    #   load the discriminator model
    discriminatorLayer = load_model(filepath=discrimininator_model_file)

    #   load the generator model
    generatorLayer = load_model(filepath=generator_model_file)

    if str(sys.argv[1]) == "all":
        # batch_size = training_set_mat.shape[0]
        batch_size_G = "all"
    else:
        batch_size_G = int(sys.argv[1])

    number_of_epochs_G = int(sys.argv[2])

    if str(sys.argv[3]) == "all":
        # batch_size = training_set_mat.shape[0]
        batch_size_C = "all"
    else:
        batch_size_C = int(sys.argv[3])

    number_of_epochs_C = int(sys.argv[4])

    alpha_w = sys.argv[5]
    beta_w = sys.argv[6]

    learning_rate =sys.argv[9]

    result_folder = result_folder + "results_adversarial_bsG_COND_DiffDist_" + str(batch_size_G) + \
                    "_noeG_" + str(number_of_epochs_G) + "_bsC_" + str(batch_size_C) + "_noeC_" + \
                    str(number_of_epochs_C) + "_alpha_" + alpha_w + "_beta_" + beta_w + "_learning_rate_" + \
                    learning_rate + "/"

    if not os.path.isdir(result_folder):
        os.makedirs(result_folder)
        os.makedirs(result_folder + "datasets/")

    ########################################################################################
    ##############################  vactors to store plot values  ##########################
    ########################################################################################

    discriminator_epochs = []
    discriminator_loss_vec = []
    discriminator_categ_acc_vec = []
    discriminator_val_loss_vec = []
    discriminator_val_categ_acc_vec = []

    adv_epochs = []
    adv_loss = []
    adv_gen_ANN_loss_vec = []
    adv_val_gen_ANN_loss_vec = []
    adv_discr_loss_vec = []
    #adv_discr_categ_acc_vec = []
    adv_val_discr_loss = []
    #adv_val_disc_categ_acc = []

    evaluation_adv_net = []
    evaluation_adv_net_2 = []

    ################################################################################################################
    conf_mat_list_tr = []
    accuracy_list_tr = []
    precision_list_tr = []
    recall_list_tr = []
    f1_list_tr = []

    conf_mat_list_val = []
    accuracy_list_val = []
    precision_list_val = []
    recall_list_val = []
    f1_list_val = []

    conf_mat_list_ts = []
    accuracy_list_ts = []
    precision_list_ts = []
    recall_list_ts = []
    f1_list_ts = []
    ################################################################################################################

    ####################################################################
    ##############################  Training  ##########################
    ####################################################################

    cont = -1
    current_history_adv_discriminator_not_trainable = None
    current_history_discriminator = None

    while True:
        cont += 1
        print bcolors.OKBLUE + "Iteration " + str(cont) + bcolors.ENDC

        #   indexes for the training and the test set
        training_idx, test_idx, val_idx = datasetSplit(supervisionDiscriminator_mat=trainSupervision,
                                                       testPercentage=0, valPercentage=10)

        print len(training_idx)
        print len(test_idx)
        print len(val_idx)

        with open(result_folder + 'datasets/indices_of_the_noisy_training_set_used_for_training', 'wb') as f:
            pickle.dump(training_idx, f)

        with open(result_folder + 'datasets/indices_of_the_noisy_training_set_used_for_validation', 'wb') as f:
            pickle.dump(val_idx, f)

        #   input for the generator training
        inputGenerator_mat_train = train[training_idx, :]
        # print inputGenerator_mat_train
        #   input for the generator testing
        inputGenerator_mat_test = test
        #   input for the generator validation
        inputGenerator_mat_val = train[val_idx, :]

        #   supervision for the discriminator training weights for classes balancing
        supervisionDiscriminator_mat_train, weight_train = \
            one_hot_coding_and_weight_computation(trainSupervision[training_idx, :])

        #   supervision for the discriminator testing
        supervisionDiscriminator_mat_val = \
            one_hot_coding_and_weight_computation(trainSupervision[val_idx, :])[0]
        #   prediction of input for discriminator training
        supervisionDiscriminator_mat_test = \
            one_hot_coding_and_weight_computation(testSpervision)[0]
        #   prediction of input for discriminator training

        print "inputGenerator_mat_train ---> ", inputGenerator_mat_train.shape
        print "inputGenerator_mat_test --->", inputGenerator_mat_test.shape
        print "inputGenerator_mat_val ---> ", inputGenerator_mat_val.shape
        print "supervisionDiscriminator_mat_train ---> ", supervisionDiscriminator_mat_train.shape
        print "supervisionDiscriminator_mat_val --->", supervisionDiscriminator_mat_val.shape
        print "supervisionDiscriminator_mat_test ---> ", supervisionDiscriminator_mat_test.shape

        # print supervisionDiscriminator_mat.shape
        # print supervisionDiscriminator_mat_train.shape
        # print supervisionDiscriminator_mat_val.shape
        # print supervisionDiscriminator_mat_test.shape
        with open(result_folder + 'datasets/inputGenerator_mat_train', 'wb') as f:
            pickle.dump(inputGenerator_mat_train, f)

        with open(result_folder + 'datasets/inputGenerator_mat_test', 'wb') as f:
            pickle.dump(inputGenerator_mat_test, f)

        with open(result_folder + 'datasets/inputGenerator_mat_val', 'wb') as f:
            pickle.dump(inputGenerator_mat_val, f)

        with open(result_folder + 'datasets/supervisionDiscriminator_mat_train', 'wb') as f:
            pickle.dump(supervisionDiscriminator_mat_train, f)

        with open(result_folder + 'datasets/supervisionDiscriminator_mat_val', 'wb') as f:
            pickle.dump(supervisionDiscriminator_mat_val, f)

        with open(result_folder + 'datasets/supervisionDiscriminator_mat_test', 'wb') as f:
            pickle.dump(supervisionDiscriminator_mat_test, f)

        with open(result_folder + 'datasets/weight_train', 'wb') as f:
            pickle.dump(weight_train, f)

        ####################################################################
        ##############################  dNet  ##############################
        ####################################################################

        #   create the architecture for the discriminator where the the discriminator layer is active
        input_layer_discriminator = Input(shape=(2,), name='discriminator_Input')
        discriminatorLayer.trainable = True
        output_layer_discriminator = discriminatorLayer(input_layer_discriminator)

        #   create the discriminator model
        discriminator = Model(inputs=input_layer_discriminator, outputs=output_layer_discriminator,
                              name="discriminator")

        #   compile discriminator model
        discriminator.compile(loss='categorical_crossentropy',
                              optimizer='adam',
                              metrics=['categorical_accuracy'])

        trainable_weigths_discriminator_network = len(discriminator.trainable_weights)
        print "trainable_weigths_discriminator_network ---> ", trainable_weigths_discriminator_network
        ####################################################################
        ##############################  advNet  ############################
        ####################################################################

        #   create an input layer for the advNet, it must have the same dimension as the gNet input
        input_layer_advNet = Input(shape=(4,), name='adversarial_Input')

        #   create the architecture for the adversarial net where the the discriminator model is not active and the
        #   generative layer is active
        discriminator.trainable = False
        generatorLayer.trainable = True

        trainable_weigths_generator_network = len(generatorLayer.trainable_weights)
        print "trainable_weigths_generator_network ---> ", trainable_weigths_generator_network

        #   create advNet model architecture
        #   this model has one inout and two outputs
        #   two outputs are needed to check two loss functions
        #   the first output is the output of the generator layer
        #   the second outpu is the output of the discriminator module when it receives the generatorLayer output
        advNet = Model(input_layer_advNet, [generatorLayer(input_layer_advNet),
                                            discriminator(generatorLayer(input_layer_advNet))])

        #   we have two losses one is used to minize the mse between the input and the output of the generatorLayer while
        #   the other one is used to minize the opposite of the categorical_crossentropy so to maximize the distance between
        #   target and prediction for the classifier
        optimizer_adv = optimizers.Adam(lr=float(learning_rate))
        alpha = K.variable(float(sys.argv[5]))
        beta = K.variable(float(sys.argv[6]))
        loss_weights = [alpha, beta]
        advNet.compile(loss=[adv_euclid_280_meters, MI_loss],
                       loss_weights=loss_weights,
                       optimizer=optimizer_adv)  # ,
        # metrics=['categorical_accuracy'])

        trainable_weigths_adversarial_network = len(advNet.trainable_weights)
        print "trainable_weigths_adversarial_network ---> ", trainable_weigths_adversarial_network

        print discriminator.summary()
        print advNet.summary()

        print "trainable_weigths_discriminator_network ---> ", trainable_weigths_discriminator_network
        print "trainable_weigths_generator_network ---> ", trainable_weigths_generator_network
        print "trainable_weigths_adversarial_network ---> ", trainable_weigths_adversarial_network

        discriminator_tr = advNet.predict(x=inputGenerator_mat_train)[0]

        #   prediction of input for discriminator validation
        discriminator_valid = advNet.predict(x=inputGenerator_mat_val)[0]

        print bcolors.OKGREEN + "Training discriminator" + bcolors.ENDC
        if batch_size_C == "all":
            batch_size_C = discriminator_tr.shape[0]

        current_history_discriminator = discriminator.fit(x=discriminator_tr,
                                                          y=supervisionDiscriminator_mat_train,
                                                          batch_size=batch_size_C,
                                                          epochs=number_of_epochs_C,
                                                          # class_weight=weight_train,
                                                          validation_data=
                                                          (discriminator_valid, supervisionDiscriminator_mat_val))  # ,
        # callbacks=[callback_discriminator])
        # callbacks=[callback_discriminator,callback_tensorboard_discriminator])

        #   update vectors with measures for the discriminator
        discriminator_epochs.append(len(current_history_discriminator.history.get('loss')))
        for i in range(0, len(current_history_discriminator.history.get('loss'))):
            discriminator_loss_vec.append(
                current_history_discriminator.history.get('loss')[i])
            discriminator_categ_acc_vec.append(
                current_history_discriminator.history.get('categorical_accuracy')[i])
            discriminator_val_loss_vec.append(
                current_history_discriminator.history.get('val_loss')[i])
            discriminator_val_categ_acc_vec.append(
                current_history_discriminator.history.get('val_categorical_accuracy')[i])

        eval = advNet.evaluate(x=inputGenerator_mat_test,
                               y=[inputGenerator_mat_test[:, 0:2], supervisionDiscriminator_mat_test],
                               batch_size=inputGenerator_test_mat.shape[0])
        print eval

        eval2 = discriminator.evaluate(x=generatorLayer.predict(inputGenerator_mat_test),
                                       y=supervisionDiscriminator_mat_test,
                                       batch_size=generatorLayer.predict(inputGenerator_mat_test).shape[0])

        ################################################################################################################
        training_set_prediction = discriminator.predict(x=discriminator_tr)
        training_set_prediction = training_set_prediction.argmax(axis=-1)

        conf_mat_list_tr.append(compute_confusion_matric(y_classes=supervisionDiscriminator_mat_train.argmax(axis=-1),
                                                         y_pred_classes=training_set_prediction))

        accuracy_list_tr.append(compute_accuracy(y_classes=supervisionDiscriminator_mat_train.argmax(axis=-1),
                                                 y_pred_classes=training_set_prediction))

        precision_list_tr.append(compute_precision(y_classes=supervisionDiscriminator_mat_train.argmax(axis=-1),
                                                   y_pred_classes=training_set_prediction))

        recall_list_tr.append(compute_recall(y_classes=supervisionDiscriminator_mat_train.argmax(axis=-1),
                                             y_pred_classes=training_set_prediction))

        f1_list_tr.append(compute_f1_score(y_classes=supervisionDiscriminator_mat_train.argmax(axis=-1),
                                           y_pred_classes=training_set_prediction))


        validation_set_prediction = discriminator.predict(x=discriminator_valid)
        validation_set_prediction = validation_set_prediction.argmax(axis=-1)

        conf_mat_list_val.append(compute_confusion_matric(y_classes=supervisionDiscriminator_mat_val.argmax(axis=-1),
                                                         y_pred_classes=validation_set_prediction))

        accuracy_list_val.append(compute_accuracy(y_classes=supervisionDiscriminator_mat_val.argmax(axis=-1),
                                                 y_pred_classes=validation_set_prediction))

        precision_list_val.append(compute_precision(y_classes=supervisionDiscriminator_mat_val.argmax(axis=-1),
                                                   y_pred_classes=validation_set_prediction))

        recall_list_val.append(compute_recall(y_classes=supervisionDiscriminator_mat_val.argmax(axis=-1),
                                             y_pred_classes=validation_set_prediction))

        f1_list_val.append(compute_f1_score(y_classes=supervisionDiscriminator_mat_val.argmax(axis=-1),
                                           y_pred_classes=validation_set_prediction))

        test_set_prediction = discriminator.predict(x=advNet.predict(inputGenerator_mat_test)[0])
        test_set_prediction = test_set_prediction.argmax(axis=-1)

        conf_mat_list_ts.append(compute_confusion_matric(y_classes=supervisionDiscriminator_mat_test.argmax(axis=-1),
                                                          y_pred_classes=test_set_prediction))

        accuracy_list_ts.append(compute_accuracy(y_classes=supervisionDiscriminator_mat_test.argmax(axis=-1),
                                                  y_pred_classes=test_set_prediction))

        precision_list_ts.append(compute_precision(y_classes=supervisionDiscriminator_mat_test.argmax(axis=-1),
                                                    y_pred_classes=test_set_prediction))

        recall_list_ts.append(compute_recall(y_classes=supervisionDiscriminator_mat_test.argmax(axis=-1),
                                              y_pred_classes=test_set_prediction))

        f1_list_ts.append(compute_f1_score(y_classes=supervisionDiscriminator_mat_test.argmax(axis=-1),
                                            y_pred_classes=test_set_prediction))


        ################################################################################################################

        # print discriminator_epochs
        # print discriminator_loss_vec
        # print discriminator_categ_acc_vec
        # print discriminator_val_loss_vec
        # print discriminator_val_categ_acc_vec
        """
        if current_history_discriminator is not None and \
                current_history_discriminator.history.get('val_categorical_accuracy')[-1] < 0.35 and \
                current_history_adv_discriminator_not_trainable is not None and \
                current_history_adv_discriminator_not_trainable.history.get('generatorANN_loss')[-1] <= 50.0:
            advNet.save_weights(
                result_folder + '/adv_net_model_WEIGHTS_after_adversarial_training' + str(cont))
        """

        advNet.save_weights(result_folder + '/adv_net_model_WEIGHTS_after_adversarial_training' + str(cont))

        print bcolors.OKGREEN + "Training adversarial" + bcolors.ENDC

        if batch_size_G == "all":
            batch_size_G = inputGenerator_mat_train.shape[0]

        current_history_adv_discriminator_not_trainable = advNet.fit(x=inputGenerator_mat_train,
                                                                     y=[inputGenerator_mat_train[:, 0:2],
                                                                        supervisionDiscriminator_mat_train],
                                                                     batch_size=batch_size_G,
                                                                     epochs=number_of_epochs_G,
                                                                     validation_data=(inputGenerator_mat_val,
                                                                                      [inputGenerator_mat_val[:, 0:2],
                                                                                       supervisionDiscriminator_mat_val]))  # ,
        # callbacks=[call_back_gLoss_upper(alpha=alpha,
        #                                 beta=beta)])
        # callbacks=[call_back_gLoss_upper(alpha=alpha,
        #                                 beta=beta)])
        # callbacks=[callBack_gen_loss()])
        # callbacks=[callback_adv_loss,
        #           call_back_gLoss_upper(alpha=alpha,
        #                                 beta=beta)])

        #   update vectors with measures for the adv
        adv_epochs.append(len(current_history_adv_discriminator_not_trainable.history.get('loss')))
        for i in range(0, len(current_history_adv_discriminator_not_trainable.history.get('loss'))):
            adv_loss.append(current_history_adv_discriminator_not_trainable.history.get('loss')[i])
            adv_gen_ANN_loss_vec.append(
                current_history_adv_discriminator_not_trainable.history.get('generatorANN_loss')[i])
            adv_val_gen_ANN_loss_vec.append(
                current_history_adv_discriminator_not_trainable.history.get('val_generatorANN_loss')[i])
            adv_discr_loss_vec.append(
                current_history_adv_discriminator_not_trainable.history.get('discriminator_loss')[i])
            #adv_discr_categ_acc_vec.append(
            #    current_history_adv_discriminator_not_trainable.history.get('discriminator_categorical_accuracy')[i])
            adv_val_discr_loss.append(
                current_history_adv_discriminator_not_trainable.history.get('val_discriminator_loss')[i])
            #adv_val_disc_categ_acc.append(
            #    current_history_adv_discriminator_not_trainable.history.get('val_discriminator_categorical_accuracy')[
            #        i])

        #   save all vectors
        with open(result_folder + 'discriminator_epochs.pkl', 'wb') as f:
            pickle.dump(discriminator_epochs, f)
        with open(result_folder + 'discriminator_loss_vec.pkl', 'wb') as f:
            pickle.dump(discriminator_loss_vec, f)
        with open(result_folder + 'discriminator_categ_acc_vec.pkl', 'wb') as f:
            pickle.dump(discriminator_categ_acc_vec, f)
        with open(result_folder + 'discriminator_val_loss_vec.pkl', 'wb') as f:
            pickle.dump(discriminator_val_loss_vec, f)
        with open(result_folder + 'discriminator_val_categ_acc_vec.pkl', 'wb') as f:
            pickle.dump(discriminator_val_categ_acc_vec, f)

        with open(result_folder + 'adv_epochs.pkl', 'wb') as f:
            pickle.dump(adv_epochs, f)
        with open(result_folder + 'adv_loss.pkl', 'wb') as f:
            pickle.dump(adv_loss, f)
        with open(result_folder + 'adv_gen_ANN_loss_vec.pkl', 'wb') as f:
            pickle.dump(adv_gen_ANN_loss_vec, f)
        with open(result_folder + 'adv_val_gen_ANN_loss_vec.pkl', 'wb') as f:
            pickle.dump(adv_val_gen_ANN_loss_vec, f)
        with open(result_folder + 'adv_discr_loss_vec.pkl', 'wb') as f:
            pickle.dump(adv_discr_loss_vec, f)
        #with open(result_folder + 'adv_discr_categ_acc_vec.pkl', 'wb') as f:
        #    pickle.dump(adv_discr_categ_acc_vec, f)
        with open(result_folder + 'adv_val_discr_loss.pkl', 'wb') as f:
            pickle.dump(adv_val_discr_loss, f)
        #with open(result_folder + 'adv_val_disc_categ_acc.pkl', 'wb') as f:
        #    pickle.dump(adv_val_disc_categ_acc, f)

        with open(result_folder + 'conf_mat_list_tr.pkl', 'wb') as f:
            pickle.dump(conf_mat_list_tr, f)
        with open(result_folder + 'accuracy_list_tr.pkl', 'wb') as f:
            pickle.dump(accuracy_list_tr, f)
        with open(result_folder + 'precision_list_tr.pkl', 'wb') as f:
            pickle.dump(precision_list_tr, f)
        with open(result_folder + 'recall_list_tr.pkl', 'wb') as f:
            pickle.dump(recall_list_tr, f)
        with open(result_folder + 'f1_list_tr.pkl', 'wb') as f:
            pickle.dump(f1_list_tr, f)
        with open(result_folder + 'conf_mat_list_val.pkl', 'wb') as f:
            pickle.dump(conf_mat_list_val, f)
        with open(result_folder + 'accuracy_list_val.pkl', 'wb') as f:
            pickle.dump(accuracy_list_val, f)
        with open(result_folder + 'precision_list_val.pkl', 'wb') as f:
            pickle.dump(precision_list_val, f)
        with open(result_folder + 'recall_list_val.pkl', 'wb') as f:
            pickle.dump(recall_list_val, f)
        with open(result_folder + 'f1_list_val.pkl', 'wb') as f:
            pickle.dump(f1_list_val, f)
        with open(result_folder + 'conf_mat_list_ts.pkl', 'wb') as f:
            pickle.dump(conf_mat_list_ts, f)
        with open(result_folder + 'accuracy_list_ts.pkl', 'wb') as f:
            pickle.dump(accuracy_list_ts, f)
        with open(result_folder + 'precision_list_ts.pkl', 'wb') as f:
            pickle.dump(precision_list_ts, f)
        with open(result_folder + 'recall_list_ts.pkl', 'wb') as f:
            pickle.dump(recall_list_ts, f)
        with open(result_folder + 'f1_list_ts.pkl', 'wb') as f:
            pickle.dump(f1_list_ts, f)


        """
        advNet.save_weights(
            result_folder + '/adv_net_model_WEIGHTS_after_adversarial_training')
        """

        evaluation_adv_net.append(eval)
        with open(result_folder + 'evaluation_adv_net.pkl', 'wb') as f:
            pickle.dump(evaluation_adv_net, f)
        evaluation_adv_net_2.append(eval2)
        with open(result_folder + 'evaluation_adv_net_2.pkl', 'wb') as f:
            pickle.dump(evaluation_adv_net, f)

        #   load the discriminator model
        discriminatorLayer = load_model(filepath=discrimininator_model_file)

        #   load the generator model
        # generatorLayer = Model(advNet.inputs, advNet.output[0])
        generatorLayer = advNet.layers[1]
        # print generatorLayer.summary()


#   to run this function as standalone
if __name__ == "__main__":
    scaled_adv_training_set_df = loadDataset(
        inputFile=sys.argv[10])

    scaled_adv_test_set_df = loadDataset(
        inputFile=sys.argv[11])
    scaled_adv_training_set_mat = scaled_adv_training_set_df.values
    scaled_adv_test_set_mat = scaled_adv_test_set_df.values

    colNames = scaled_adv_training_set_df.columns.values

    train_original_lat = pn.DataFrame(data=scaled_adv_training_set_mat[:, 0],
                                      columns=[colNames[0]])
    train_original_lon = pn.DataFrame(data=scaled_adv_training_set_mat[:, 1],
                                      columns=[colNames[1]])
    train_angle_seed = pn.DataFrame(data=scaled_adv_training_set_mat[:, 2],
                                    columns=[colNames[2]])
    train_radius_seed = pn.DataFrame(data=scaled_adv_training_set_mat[:, 3],
                                     columns=[colNames[3]])
    train_noisy_lat = pn.DataFrame(data=scaled_adv_training_set_mat[:, 4],
                                   columns=[colNames[4]])
    train_noisy_lon = pn.DataFrame(data=scaled_adv_training_set_mat[:, 5],
                                   columns=[colNames[5]])
    train_user_id = pn.DataFrame(data=scaled_adv_training_set_mat[:, 6],
                                 columns=[colNames[6]])

    input_adv_train_df = pn.concat(objs=
                                   [train_original_lat, train_original_lon, train_angle_seed, train_radius_seed],
                                   axis=1)

    inputGenerator_train_mat = input_adv_train_df.values

    # for i in range(0, 20):
    #    print scaled_adv_training_set_mat[i, 0:4], " <----> ", inputGenerator_train_mat[i, :]

    test_original_lat = pn.DataFrame(data=scaled_adv_test_set_mat[:, 0],
                                     columns=[colNames[0]])
    test_original_lon = pn.DataFrame(data=scaled_adv_test_set_mat[:, 1],
                                     columns=[colNames[1]])
    test_angle_seed = pn.DataFrame(data=scaled_adv_test_set_mat[:, 2],
                                   columns=[colNames[2]])
    test_radius_seed = pn.DataFrame(data=scaled_adv_test_set_mat[:, 3],
                                    columns=[colNames[3]])
    test_noisy_lat = pn.DataFrame(data=scaled_adv_test_set_mat[:, 4],
                                  columns=[colNames[4]])
    test_noisy_lon = pn.DataFrame(data=scaled_adv_test_set_mat[:, 5],
                                  columns=[colNames[5]])
    test_user_id = pn.DataFrame(data=scaled_adv_test_set_mat[:, 6],
                                columns=[colNames[6]])

    input_adv_test_df = pn.concat(objs=
                                  [test_original_lat, test_original_lon, test_angle_seed, test_radius_seed],
                                  axis=1)

    inputGenerator_test_mat = input_adv_test_df.values

    # print inputGenerator_train_mat.shape
    # print inputGenerator_test_mat.shape

    trainSupervision_df = pn.DataFrame(data=scaled_adv_training_set_mat[:, 6],
                                       columns=['trainSupervision'])
    testSupervision_df = pn.DataFrame(data=scaled_adv_test_set_mat[:, 6],
                                      columns=['trainSupervision'])

    trainSupervision_mat = trainSupervision_df.values
    testSupervision_mat = testSupervision_df.values

    adversarialModel(train=inputGenerator_train_mat,
                     trainSupervision=trainSupervision_mat,
                     test=inputGenerator_test_mat,
                     testSpervision=testSupervision_mat,
                     discrimininator_model_file=sys.argv[12],
                     generator_model_file=sys.argv[13],
                     id_GPU=sys.argv[7],
                     memory_percentage=float(sys.argv[8]),
                     result_folder=sys.argv[14])
